
#ifndef UTILS_H
#define UTILS_H

#include <eigen3/Eigen/Dense>
#include <cmath>
#include <vector>
#include <string>
#include <iostream>

//int NORTH = 0;
//int EAST = 1;
//int DEPTH = 2;

class NED {
public:
    NED(double lat, double lon, double height) {
        // Create a NED plane centered at lat, lon, altitude
        // Constants defined by the World Geodetic System 1984 (WGS84)
        a = 6378137;
        b = 6356752.3142;
        esq = 6.69437999014 * 0.001;
        e1sq = 6.73949674228 * 0.001;
        f = 1 / 298.257223563;

        // Save NED origin
        init_lat = degree2radian(lat);
        init_lon = degree2radian(lon);
        init_h = height;
        init_ecef = geodetic2ecef(lat, lon, height);
        phiP = atan2(init_ecef[2], sqrt(pow(init_ecef[0],2) +  pow(init_ecef[1],2)) );
        ecef_to_ned_matrix = nRe(phiP, init_lon);
        ned_to_ecef_matrix = nRe(init_lat, init_lon).transpose();

    }
    Eigen::Vector3d geodetic2ecef(double latitude, double longitude, double height) {
        //Convert geodetic coordinates to ECEF."""
        //latitude in degrees, longitude in degress, height in meters
        // http://code.google.com/p/pysatel/source/browse/trunk/coord.py?r=22

        Eigen::Vector3d result;

        double lat = degree2radian(latitude);
        double lon = degree2radian(longitude);

        double xi = sqrt(1 - esq * pow(sin(lat),2));
        double x = (a / xi + height) * cos(lat) * cos(lon);
        double y = (a / xi + height) * cos(lat) * sin(lon);
        double z = (a / xi * (1 - esq) + height) * sin(lat);
        result << x, y, z;
        return result;
    }

    Eigen::Vector3d ecef2geodetic(Eigen::Vector3d ecef) {
        // Convert ECEF coordinates to geodetic.
        /* J. Zhu, "Conversion of Earth-centered Earth-fixed coordinates \
         to geodetic coordinates," IEEE Transactions on Aerospace and \
         Electronic Systems, vol. 30, pp. 957-961, 1994
         */

        Eigen::Vector3d result;

        double x = ecef[0];
        double y = ecef[1];
        double z = ecef[2];

        double r = sqrt(x * x + y * y);
        double Esq = a * a - b * b;
        double F = 54 * b * b * z * z;
        double G = r * r + (1 - esq) * z * z - esq * Esq;
        double C = (esq * esq * F * r * r) / (pow(G, 3));
        double S = cbrt(1 + C + sqrt(C * C + 2 * C));
        double P = F / (3 * pow((S + 1 / S + 1), 2) * G * G);
        double Q = sqrt(1 + 2 * esq * esq * P);
        double r_0 =  -(P * esq * r) / (1 + Q) + sqrt(0.5 * a * a*(1 + 1.0 / Q) -
            P * (1 - esq) * z * z / (Q * (1 + Q)) - 0.5 * P * r * r);
        double U = sqrt(pow((r - esq * r_0), 2) + z * z);
        double V = sqrt(pow((r - esq * r_0), 2) + (1 - esq) * z * z);
        double Z_0 = b * b * z / (a * V);
        double h = U * (1 - b * b / (a * V));
        double lat = atan((z + e1sq * Z_0) / r);
        double lon = atan2(y, x);
        result << radian2degree(lat), radian2degree(lon), h;
        return result;

       }

     Eigen::Vector3d ecef2ned(Eigen::Vector3d ecef) {
        /*Converts ECEF coordinate pos into local-tangent-plane ENU
        coordinates relative to another ECEF coordinate ref. Returns a tuple
        (East, North, Up).
        */

        Eigen::Vector3d p = ecef - init_ecef;
        Eigen::Vector3d ned = ecef_to_ned_matrix*p;
        ned[2] = -ned[2];
        return ned;

     }

     Eigen::Vector3d ned2ecef(Eigen::Vector3d ned) {
        //NED (north/east/down) to ECEF coordinate system conversion.
        ned[2] =  -ned[2];
        Eigen::Vector3d res = ned_to_ecef_matrix*ned + init_ecef;
        return res;
}

     Eigen::Vector3d geodetic2ned(Eigen::Vector3d coord){
        // Geodetic position to a local NED system
        Eigen::Vector3d ecef = geodetic2ecef(coord[0],coord[1],coord[2]);
        return ecef2ned(ecef);
    }

    Eigen::Vector3d ned2geodetic(Eigen::Vector3d ned) {
        // Local NED position to geodetic
        Eigen::Vector3d ecef =  ned2ecef(ned);
        return  ecef2geodetic(ecef);
    }

    Eigen::Vector2d degree2DegreeMinute(double lat, double lon) {
        double lat_degree = degree2DegreeMinuteAux(lat);
        double lon_degree = degree2DegreeMinuteAux(lon);
        Eigen::Vector2d result;
        result << lat_degree, lon_degree;
        return result;
    }

     static Eigen::Vector2d degreeMinute2Degree(double lat,double lon){
       /* Transforms latitude and longitude in the format
            DDDMM.MM to the format DDD.DD */
        Eigen::Vector2d latdegmin = splitDegreeMinutes(lat);
        double lat_deg = latdegmin[0];
        double lat_min = latdegmin[1];

        Eigen::Vector2d londegmin = splitDegreeMinutes(lon);
        double lon_deg = londegmin[0];
        double lon_min = londegmin[1];

        Eigen::Vector2d result;
        result << lat_deg + lat_min/60.0, lon_deg + lon_min/60.0;
        return result;
}
        double degree2radian(double degrees) {
            return (degrees * M_PI) / 180 ;
        }

        double radian2degree(double radians) {
             return (radians * 180) / M_PI ;
        }
     
     static double degreeMinute2Degree(double lat){
        Eigen::Vector2d latdegmin = splitDegreeMinutes(lat);
        double lat_deg = latdegmin[0];
        double lat_min = latdegmin[1];
        return (lat_deg + lat_min/60.0);
     }

   // Converts decimal degrees to degree minute 
    static double degree2DegreeMinuteAux(double value){
        //val = str(value).split('.');
        //minute = float('0.' + val[1]) * 60.0;
        int degrees = floor(value);
        float minutes = (value - degrees) * 60;
        std::string deg_str = std::to_string(degrees);
        std::string min_str = std::to_string(minutes);

        if (minutes < 10.0)
            return std::stod(deg_str + '0'+ min_str);
        else
            return std::stod(deg_str + min_str);

    }


private:

    Eigen::Matrix3d nRe(double lat, double lon) {

        double sinLat = sin(lat);
        double sinLon = sin(lon);
        double cosLat = cos(lat);
        double cosLon = cos(lon);

        Eigen::Matrix3d mx;
        mx << -sinLat*cosLon,   -sinLat*sinLon,    cosLat ,
              -sinLon,           cosLon,           .0    ,
               cosLat*cosLon,   cosLat*sinLon,      sinLat;

        return mx;
    }

    double cbrt(double x) {
        if (x >= 0) {
            return pow(x, 1.0/3.0);
        }
        else {
            return -pow(abs(x), 1.0/3.0);
        }
    }

 

    static Eigen::Vector2d splitDegreeMinutes (double value) {


        Eigen::Vector2d result;

        double degree = (int) (value / 100) ;
        double minutes = value - degree * 100 ;
        result <<  degree, minutes;
        return result;

    }

    double a, b, esq, e1sq, f, phiP;
    double init_lat, init_lon, init_h;
    Eigen::Vector3d init_ecef;

    Eigen::Matrix3d ecef_to_ned_matrix, ned_to_ecef_matrix;

    };

#endif
