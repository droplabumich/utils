
#include "utils/NED.h"

using namespace Eigen;

int main(void) {
    // Test example

    double lat0, lat1, lat2, lat3, lat4, lat5;
    double lon0, lon1, lon2, lon3, lon4, lon5;

    lat0 = 4303.4739;   //DDDMM.MMMM
    lon0 = 600.5378;    //DDDMM.MMMM
    lat1 = 43.05760;    //DDD.DDDDD
    lon1 = 6.007583;    //DDD.DDDDD
    lat2 = 43.057417;   //DDD.DDDDD
    lon2 = 6.007667;    //DDD.DDDDD
    lat3 = 39.997417;   //DDD.DDDDD
    lon3 = 6.007667;    //DDD.DDDDD
    lat4 = 40.017417;   //DDD.DDDDD
    lon4 = 6.00667;     //DDD.DDDDD
    lat5 = 39.9717417;  //DDD.DDDDD
    lon5 = 6.01667;     //DDD.DDDDD

       // lat_0, lon_0 = degreeMinute2Degree(lat0, lon0)
       // ned = NED(lat0, lon0, 0.0)
      // print ned.llh2Eecef([lat1, lon1, 0.0])

       NED ned(lat3, lon3, 0.0);
       Vector3d ned_1 = ned.geodetic2ned(Vector3d(lat4, lon4, 0.0));
       std::cout <<"NED1: "<< ned_1.transpose() << std::endl;
       Vector3d ned_2 = ned.geodetic2ned(Vector3d(lat5, lon5, 0.0));
       std::cout <<"NED2: "<< ned_2.transpose() << std::endl;
       Vector3d geod = ned.ned2geodetic(ned_2);
       std::cout <<"Geodetic: " <<geod.transpose() << std::endl;

       std::cout << sqrt(pow((ned_1[0] - ned_2[0]),2) + pow((ned_1[1] - ned_2[1]),2));
       return 0;
}
