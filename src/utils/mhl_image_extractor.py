#!/usr/bin/python

# Start up ROS pieces.
from __future__ import print_function

import rosbag
import datetime
import os
from cv_bridge import CvBridge
import cv2
import argparse


class ImageCreator():
    # Must have __init__(self) function for a class, similar to a C++ class constructor.
    def __init__(self,filename, output=None):

        # Use a CvBridge to convert ROS images to OpenCV images so they can be saved.
        self.bridge = CvBridge()



        self.bag_name_we = os.path.basename(filename)
        self.bag_name = os.path.splitext(self.bag_name_we)[0]
        self.bag_dir = os.path.dirname(filename)

        # Choose output directory
        if output is None:
            self.output_dir = os.path.join(self.bag_dir, self.bag_name+'_images/')
        else:
            if not os.path.isdir(output):
                print("Output must be a path")
                exit(-1)
            self.output_dir = output

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

        # Open bag file.
        exported = 0
        with rosbag.Bag(filename, 'r') as bag:
            print("Exporting ",filename, " to ", self.output_dir)
            self.image_left_count = bag.get_message_count(["/camera1/image_raw"])
            self.image_right_count = bag.get_message_count(["/camera2/image_raw"])
            for topic, msg, t in bag.read_messages():
                if topic == "/camera1/image_raw" :
                    self.camera_callback(msg, "LC08")
                    exported += 1
                if topic == "/camera2/image_raw":
                    self.camera_callback(msg, "RC08")
                    exported += 1
                print("\rExported: ",exported,"/",self.image_left_count+self.image_right_count,end='')
        print("\nFinished extracting images from bag")


                
    def parseBagName(self):

        # Get the date from the current bag file name
        splitted_date = list(int(x) for x in self.bag_name_we[-23:-13].split('-'))
        splitted_time = list(int(x) for x in self.bag_name_we[-12:-4].split('-'))

        date = datetime.datetime(splitted_date[0], splitted_date[1], splitted_date[2], splitted_time[0], splitted_time[1], splitted_time[2])

        return date

    def camera_callback(self, data, name):
        # Convert image to cv2
        cv2_img = self.bridge.imgmsg_to_cv2(data, "passthrough")
        if data.encoding == 'bayer_rggb16':
            cv2_img = cv2_img*16

        cv2_img = cv2.cvtColor(cv2_img, cv2.COLOR_BAYER_RG2RGB)

        if data.encoding == 'bayer_rggb16':
            cv2_img = (cv2_img/256).astype('uint8')

        # Create output data string
        secs = str(data.header.stamp.secs)[4:]
        nsecs = str(data.header.stamp.nsecs)
        nsecs3 = int(nsecs)/(10**6)

        nsecs_s = "_%03d_" % nsecs3
        filename1 = 'PR_20180121_' + secs + nsecs_s +name+".png"

        # Save image
        cv2.imwrite(os.path.join(self.output_dir + filename1), cv2_img)




def getImageNames(directory,filetype):
    if os.path.isfile(directory):
        return [directory]

    templist = os.listdir(directory)
    filelist = []
    #print "Templist in "+str(directory)+" folder: "+str(templist)
    for name in templist:
        if os.path.isfile(os.path.join(directory, name)):
            if os.path.splitext(name)[1] in filetype:
                filelist.append(os.path.join(directory, name))
            else:
                pass
                #print("Not supported format: " + str(os.path.splitext(name)[1]))
        elif os.path.isdir(os.path.join(directory, name)):
            filelist += getImageNames(os.path.join(directory, name),filetype)
        else:
            print("Not supported file type")

    if len(filelist)== 0:
        print("No valid files in directory "+str(directory))

    return filelist




# Main function.
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Extract stereo images from bagfile')
    parser.add_argument('file_dir', help='Bag file to extract images from')
    parser.add_argument('--output', help='Output directory for images, defaults to bag_directory/[bag_name_images', required=False, default=None)
    parser.add_argument('-r', help='Search folder and subfolder for bagfiles and process all', action="store_true", required=False, default=False)

    args = parser.parse_args()

    #image_creator = ImageCreator("/media/cenote-1/droplab/Hawaii2018/diver_rig_28jan2018/test1_open_water_28Jan/sensors_2010-05-20-23-14-00_0.bag")

    if args.r:
        # Check input arguments
        if not os.path.isdir(args.file_dir):
            print("Path needs to be a directory")
            exit(-1)

        filelist =  getImageNames(args.file_dir,['.bag'])

        for bag in filelist:
            image_creator = ImageCreator(bag, output=args.output)
    else:
        if not os.path.isfile(args.file_dir):
            print("Path needs to be a file")
            exit(-1)
        ImageCreator(args.file_dir, output=args.output)