#!/usr/bin/env python

## INPUT : PoseGraph data, file to save
## REQUIRES : PoseGraph is a valid data structure of the type utils.msg.PoseGraphUnified
## EFFECTS: publishes the data to the given file in a preset format
#
## EXCEPTION: IOException is raised when file cannot be opened due to any reason

import collections

def savePoseGraphAsCSV(data, filename):

    ## attempt to open the file
    f_out = open(filename, 'w')

    cov3d = ['cov_' + i + j for i in ['x', 'y', 'z'] for j in ['x', 'y', 'z']]
    cov2d = ['cov_' + i + j for i in ['x', 'y'] for j in ['x', 'y']]

    path_node_header = ['timestamp', 'x', 'y', 'z']
    path_node_header += cov3d
    gps_factor_header = ['timestamp', 'north', 'east']
    gps_factor_header += cov2d
    range_factor_header = ['timestamp', 'r', 'cov']

    ## dictionary to maintain a list of all keys
    all_keys_unordered = {}

    ## add all path node keys to the dictionary
    for i in range(len(data.path_keys)):
        all_keys_unordered[data.path_keys[i]] = {}
        all_keys_unordered[data.path_keys[i]]['p'] = (data.path_stamps[i], data.path.poses[i])

    ## add all node covariances to the dictionary
    for node_cov in data.path_covariance:
        cov_key = node_cov.key
        if cov_key not in all_keys_unordered:
            all_keys_unordered[cov_key] = {}
        all_keys_unordered[cov_key]['c'] = node_cov

    ## associate all gps to the all_keys_unordered dictionary
    for gps_factor in data.gps_factors:
        gps_key = gps_factor.key
        if gps_key not in all_keys_unordered:
            all_keys_unordered[gps_key] = {}
        all_keys_unordered[gps_key]['g'] = gps_factor

    ## add all range factor to the all_keys_unordered dictionary
    for range_factor in data.range_factors:
        range_key = range_factor.recv_key
        if range_key not in all_keys_unordered:
            all_keys_unordered[range_key] = {}
        all_keys_unordered[range_key]['r'] = range_factor

    header = 'ID'
    for elem in path_node_header:
        header += ',' + elem
    for elem in gps_factor_header:
        header += ',' + elem
    for elem in range_factor_header:
        header += ',' + elem
    header += '\n'

    f_out.write(header)

    # all_keys = all_keys_unordered
    all_keys = collections.OrderedDict(sorted(all_keys_unordered.items()))

    ## iterate through all keys and print some output for each one of them
    for key in all_keys:
        dataline = str(key)

        ## add information about the pose
        pose_dataline = ',NaN,NaN,NaN,NaN'
        if 'p' in all_keys[key]:
            path_node = all_keys[key]['p']
            pose_dataline = ',%f,%.6f,%.6f,%.6f' % (path_node[0],
                                                    path_node[1].pose.position.x,
                                                    path_node[1].pose.position.y,
                                                    path_node[1].pose.position.z)
        dataline += pose_dataline

        ## add information about the node covariance
        path_cov_dataline = ',NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN'
        if('c' in all_keys[key]):
            path_cov = all_keys[key]['c']
            path_cov_dataline = ',%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f' % \
                                    (path_cov.covariance[0], path_cov.covariance[1], path_cov.covariance[2],
                                     path_cov.covariance[3], path_cov.covariance[4], path_cov.covariance[5],
                                     path_cov.covariance[6], path_cov.covariance[7], path_cov.covariance[8])
        dataline += path_cov_dataline

        ## add information about the gps factor
        gps_factor_dataline = ',NaN,NaN,NaN,NaN,NaN,NaN,NaN'
        if('g' in all_keys[key]):
            gps_factor = all_keys[key]['g']
            gps_factor_dataline = ',%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f' % (gps_factor.stamp,
                                    gps_factor.north, gps_factor.east,
                                    gps_factor.covariance[0], gps_factor.covariance[1],
                                    gps_factor.covariance[2], gps_factor.covariance[3])
        dataline += gps_factor_dataline

        ## add information about the range factor
        range_factor_dataline = ',NaN,NaN,NaN,NaN'
        if ('r' in all_keys[key]):
            range_factor = all_keys[key]['r']
	    #print range_factor.sender_id
            range_factor_dataline = ',%.6f,%.6f,%.6f,%.6f' % (range_factor.stamp, int(range_factor.sender_id), range_factor.val, range_factor.var)
        dataline += range_factor_dataline

        dataline += '\n'

        f_out.write(dataline)

    f_out.close()

