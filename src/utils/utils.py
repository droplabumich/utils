#!/usr/bin/env python
# Code modified/adapted from cola2_ros_lib.py @ https://bitbucket.org/udg_cirs/cola2_core/src/2232d7bfd25a2292039652b4e99d1ba229f24b25/cola2_lib/src/cola2_lib/cola2_ros_lib.py?at=master&fileviewer=file-view-default

import rospy
from math import floor, pi, sqrt


def normalizeAngle(angle):
    return wrapAngle(angle)

def wrapAngle(angle):  # Should be the default option, "wrap angle" is the proper term in English
    return (angle + ( 2.0 * pi * floor( ( pi - angle ) / ( 2.0 * pi ) ) ) )

# getRosParams:
#   obj: object for which to set its attributes
#   params: dictionary of {'key': 'value'} pairs defining the attribute
#      name that'll be set and the name of the ROS param that'll be
#      assigned to it
#   [node_name]: optional ROS node name of the node for which the params
#      are retrieved

def getRosParams(obj, params, node_name=None):
    valid_config = True
    for key in params:
        if rospy.has_param(params[key]):
            param_value = rospy.get_param(params[key])
            setattr(obj, key, param_value)
        else:
            valid_config = False
            if node_name == None:
                rospy.logfatal(params[key] + " parameter not found")
            else:
                rospy.logfatal(node_name + ": " + params[key] + " parameter not found")
    return valid_config


# getRosParamsWithDefaults:
#   obj: object for which to set its attributes
#   params: dictionary of
#      {'attr_name': {'name': 'actual_name', default: default_value}
#      pairs defining the attribute name that'll be set, the name of the
#      ROS param that'll be assigned to it and its default value if
#      the ROS param is not found on the param server
#   [node_name]: optional ROS node name of the node for which the params
#      are retrieved
def getRosParamsWithDefaults(obj, params, node_name=None):
    valid_config = True
    for key in params:
        if rospy.has_param(params[key]['name']):
            param_value = rospy.get_param(params[key]['name'])
            setattr(obj, key, param_value)
        else:
            valid_config = False
            setattr(obj, key, params[key]['default'])
            if node_name == None:
                rospy.logfatal(params[key]['name'] + " param not found")
            else:
                rospy.logfatal(node_name + ": " + params[key]['name'] + " param not found")
    return valid_config

