#!/usr/bin/python

import numpy as np
from scipy.spatial.transform import Rotation as R
from scipy import linalg as lg
from scipy.linalg import logm, expm
from numpy.linalg import matrix_power


class navySolver:
    def __init__(self):
        # print("navy solver initialized")
        return

    def calibrateR(self, optR, odoR, optT, odoT, indexToShow):

        M = np.zeros((3, 3))
        for i in indexToShow:
            rVcl = optR[i, :]
            rCam = odoR[i, :]

            M += np.outer(rVcl, rCam)

        Rx = np.matmul(lg.sqrtm(matrix_power(
            np.matmul(np.transpose(M), M), -1)), np.transpose(M))
        # print lg.det(Rx)
        # print np.matmul(Rx, np.transpose(Rx))
        # tempr = R.from_dcm(Rx)
        # print tempr.as_rotvec()

        C = np.zeros((optR.size, 3))
        d = np.zeros(optR.size)
        for i in range(0, optR.shape[0]):
            # rVcl = R.from_rotvec(np.array([rollv[i], pitchv[i], yawv[i]]))
            # print Rx.dot(np.array([rollv[i], pitchv[i], yawv[i]]))
            rotVec = Rx.dot(optR.transpose())
            trsVec = Rx.dot(optT.transpose())

            # tempR = R.from_rotvec(np.array([rollc[i], pitchc[i], yawc[i]]))
            # C.append(np.eye(3)-tempR.as_dcm())
            # d.append(np.array([xc[i], yc[i], zc[i]])-np.dot(Rx,np.array([xv[i], yv[i], zv[i]])))
            # C[3*i:3*i+3, :] = np.eye(3)-tempR.as_dcm()
            # d[3*i:3*i+3] = np.array([xc[i], yc[i], zc[i]])-Rx.dot(np.array([xv[i], yv[i], zv[i]]))

        # matC = np.array(C)
        # vecd = np.array(d)

        # bx = (np.matmul(lg.inv(np.matmul(np.transpose(C), C)), np.transpose(C))).dot(d)
        # print(bx)

        return rotVec.transpose(), trsVec.transpose()
        # print("calibrate done")
