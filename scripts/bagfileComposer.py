#!/usr/bin/env python


import time, sys, os
from ros import rosbag
import roslib
from sensor_msgs.msg import Image
import datetime
from cv_bridge import CvBridge, CvBridgeError
import cv2
import argparse

def file_search(inputdirectory, filetype):
    files=[]
    for file in os.listdir(inputdirectory):
        if file.endswith(filetype):
            files.append(os.path.join(inputdirectory,file))
    files.sort()
    return files

def timestamp2string(timestamp):
    
    #time_string = datetime.datetime.utcfromtimestamp(timestamp.to_sec()).strftime('%Y-%m-%d--%H:%M:%S')
    time_string = str(datetime.datetime.utcfromtimestamp(timestamp.to_sec()).time()).translate(None,':')
    date_string = datetime.datetime.utcfromtimestamp(timestamp.to_sec()).date().strftime("PR_%Y%d%m")

    # Label the image
    image_label = "%s_%s_%s"% (date_string, time_string[0:6], time_string[7:10])
    return image_label

def search4image(filename, image_filepaths, camera_pose):
    '''
    Camera pose either 'L' or 'R' 
    '''

    image_path = [s for s in image_filepaths if filename in s]

    if (len(image_path)==0):
        print("No matching image found for timestamp",filename)
        return None

    found_images = []
    for image in image_path:
        if os.path.basename(image)[-8] == camera_pose:
            found_images.append(image)
        
    if (len(found_images)>1):
        print("Found too many corresponding images", found_images)
        return None 
    else:
        return found_images[0]
    

def inject_images(inputBags, imageFiles, output_bag_path):
    output_bag =rosbag.Bag(output_bag_path, 'w')

    bridge = CvBridge()

    for bag in inputBags:
        input_bag =  rosbag.Bag(bag, 'r')
        count = 0
        for topic, msg, t in input_bag:
            
            
            if topic=='/stereo_in/right/camera_info':
                timestamp = msg.header.stamp
                filename = timestamp2string(timestamp)

                image_path = search4image(filename, imageFiles, 'R')
                try:
                    imgmsg = bridge.cv2_to_imgmsg(cv2.imread(image_path, cv2.IMREAD_GRAYSCALE), "mono8")
                    imgmsg.header = msg.header
                    output_bag.write('/./mono/image_raw', imgmsg, msg.header.stamp)
                    print(filename)

                except Exception as e:
                    print(e)
                    
   
            if topic == '/stereo_in/left/camera_info':
                timestamp = msg.header.stamp
                filename = timestamp2string(timestamp)

                image_path = search4image(filename, imageFiles, 'L')
                try:
                    imgmsg = bridge.cv2_to_imgmsg(cv2.cvtColor(cv2.imread(image_path, 0),cv2.COLOR_BayerRG2RGB) , "rgb8")
                    imgmsg.header = msg.header
                    output_bag.write('/./color/image_raw', imgmsg, msg.header.stamp)
                    print(filename)

                except Exception as e:
                    print(e)
                   

            output_bag.write(topic, msg, t)

        input_bag.close()
    output_bag.close()            








def main(log_path):
    # Load bag files in folder
    inputBags = file_search(log_path, '.bag')
    print(inputBags)
    # Identify subfolder containing raw images. The folders name is formatted as a date
    for dirname, dirnames, filenames in os.walk(log_path):
        for subdirname in dirnames:
            if (subdirname[:1].isdigit() and subdirname[-5] == "_" and subdirname[-10] == "_" ):
                raw_image_path = os.path.join(dirname, subdirname)
                print(raw_image_path)
        
    images = file_search(raw_image_path, '.png')

    output_bag_name = os.path.join(os.path.dirname(inputBags[0]),'combined'+os.path.basename(inputBags[0])[:-6]+'.bag')

    inject_images(inputBags, images, output_bag_name)




if __name__=="__main__":

    parser = argparse.ArgumentParser(description='Create bag with images saved as individual files')
    parser.add_argument('path',
                        help='path where the Sphere logs were recorded. Contains 1 or more bagfiles with image_info topics for right and left images, as well as a folder \
                        with the raw images. The name of the folder with the images has to start with a number.')
    
    args = parser.parse_args()
    main(args.path)