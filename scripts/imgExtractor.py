#!/usr/bin/env python

import time
import sys
import os
from ros import rosbag
import roslib
from sensor_msgs.msg import Image
import datetime
from cv_bridge import CvBridge, CvBridgeError
import cv2
import argparse
import rospy


def extract(inputBags, outputpath):
    bridge = CvBridge()
    input_bag = rosbag.Bag(inputBags, 'r')
    
    output_path_left = os.path.join(outputpath, "LC")
    output_path_right = os.path.join(outputpath, "RM")
    
    for topic, msg, t in input_bag:
        try:
            timestamp = msg.header.stamp #- #rospy.Time.from_sec(2)
            time_string = str(datetime.datetime.fromtimestamp(timestamp.to_sec()).time()).translate(None,':')
            if topic == '/stereo_in/left/image_raw':
                gray2 = bridge.imgmsg_to_cv2(msg, desired_encoding='8UC1')
                im = cv2.cvtColor(gray2, cv2.COLOR_BAYER_BG2BGR)

                # print outputpath+'/left-'+'{0:05d}'.format(countL)
                
                image_label = "%s_%s_%s"% (time.strftime("PR_%Y%d%m"), time_string[0:6], time_string[7:10])+"_LC16.png"
                
                cv2.imwrite(os.path.join(output_path_left, image_label), im)
            
            elif topic == '/stereo_in/right/image_raw':
                gray2 = bridge.imgmsg_to_cv2(msg, desired_encoding='8UC1')
                image_label = "%s_%s_%s"% (time.strftime("PR_%Y%d%m"), time_string[0:6], time_string[7:10])+"_RM16.png"

                cv2.imwrite(os.path.join(output_path_right, image_label), gray2)
        except:
            pass

    input_bag.close()

def bag_search(inputdirectory):
    bag=[]
    try:
        for file in os.listdir(inputdirectory):
            if file.endswith('.bag'):
                bag.append(inputdirectory+'/'+file)
    except OSError:
        if inputdirectory.endswith(".bag"):
            return [inputdirectory]
    print(bag)
    return bag
   

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_folder")
    parser.add_argument("output_folder")

    args = parser.parse_args()

    bag_list = bag_search(args.input_folder)

    if not os.path.exists(os.path.join(args.output_folder, "LC")):
        os.makedirs(os.path.join(args.output_folder, "LC"))
    
    if not os.path.exists(os.path.join(args.output_folder, "RM")):
        os.makedirs(os.path.join(args.output_folder, "RM"))

    for bag in bag_list:
        extract(bag, args.output_folder)
