#!/usr/bin/python

from __future__ import division
import rospy
import rosbag
import tf
import tf.transformations
import os
import datetime
from geometry_msgs.msg import TransformStamped
import numpy as np
import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.backends.backend_pdf import PdfPages
from scipy import signal as sig
from scipy.spatial.transform import Rotation as R
from plot3d_only import *

class odomLogReader:
    def __init__(self, odoLogPath, storeRT):
        self.R = []
        with open(odoLogPath) as csv_file:
            csv_reader = csv.reader(csv_file)
            line_count = 0
            for row in csv_reader:
                if line_count > 4:
                    self.R.append(
                        np.array(map(float, row[0:9])).reshape((3, 3)))
                    storeRT.odoT.append(np.array(map(float, row[9:12])))
                    # tempr = R.from_dcm(np.array(map(float, row[0:9])).reshape((3, 3)))
                    # storeRT.odoEuler.append(tempr.as_euler())
                    storeRT.odoEuler.append(tf.transformations.euler_from_matrix(
                        np.array(map(float, row[0:9])).reshape((3, 3)), 'syxz'))
                    storeRT.kp.append(np.array(map(float, row[12:14])))
                    storeRT.Matches.append(np.array(map(float, row[14:17])))
                    storeRT.reprojErr.append(map(float, row[17:18]))
                line_count += 1
                # print storeRT.odoT


class visualizer:
    def __init__(self, RT, fig3, pdfname, mask):

        self.indexToShow = []
        for i in range(0, len(mask)):
            if mask[i] == 1 and i < len(RT.optEuler):
                self.indexToShow.append(i)

        self.odoR = np.array(RT.odoEuler)
        self.odoT = np.array(RT.odoT)

        fig = plt.figure(figsize=(14, 10))
        ax1 = plt.subplot(321)
        plt.title('Errors Comparison in Translation')
        line2, = plt.plot(
            self.odoT[:, 0], 'c', linewidth=1.5)
        line2.set_label('odometry')
        plt.axhline(0, color='black')
        # plt.setp(ax1.get_xticklabels(), visible=False)
        plt.ylabel("x(m)")
        ax1.legend(loc='best')

        ax2 = plt.subplot(323, sharex=ax1)
        plt.plot(self.odoT[:, 1], 'c', linewidth=1.5)
        plt.axhline(0, color='black')
        plt.ylabel("y(m)")

        ax3 = plt.subplot(325, sharex=ax1)
        plt.plot(self.odoT[:, 2], 'c', linewidth=1.5)

        plt.axhline(0, color='black')
        plt.ylabel("z(m)")
        plt.xlabel("frame")

        ax4 = plt.subplot(322)
        plt.title('Errors Comparison in Rotation')
        plt.plot(self.odoR[:, 0], 'c', linewidth=1.5)

        plt.axhline(0, color='black')
        plt.ylabel("roll(rad)")

        ax5 = plt.subplot(324, sharex=ax1)
        plt.plot(self.odoR[:, 1], 'c', linewidth=1.5)
        plt.axhline(0, color='black')
        plt.ylabel("pitch(rad)")

        ax6 = plt.subplot(326, sharex=ax1)
        plt.plot(self.odoR[:, 2], 'c', linewidth=1.5)

        plt.axhline(0, color='black')
        plt.ylabel("yaw(rad)")
        plt.xlabel("frame")

        pp = PdfPages(pdfname)
        pp.savefig(fig)

        fig = plt.figure(figsize=(14, 4.4))
        plt.title('Reprojectin Error')
        line1, = plt.plot(RT.reprojErr[:], 'b', linewidth=1.5)
        plt.ylabel("pixels")
        plt.xlabel("frame")
        line1.set_label('Reprojection Error')
        plt.legend(loc='best')
        pp.savefig(fig)

        tempkp = []
        tempmatch = []
        for i in range(0, 99):
            # if mask[i] == 1:
            tempkp.append(RT.kp[i])
            tempmatch.append(RT.Matches[i])

        fig = plt.figure(figsize=(14, 6))
        ax1 = plt.subplot(121)
        ax1.set_yscale('log')
        plt.title('Num of Features')
        line = plt.plot(tempkp, linewidth=1.5)
        plt.ylabel("Num of features")
        plt.xlabel("frame")
        plt.legend(iter(line), ('RightFrame', 'LeftFrame'), loc='best')

        ax2 = plt.subplot(122)
        ax2.set_yscale('log')
        plt.title('Num of Matches')
        line = plt.plot(tempmatch, linewidth=1.5)
        plt.ylabel("Num of Matches")
        plt.xlabel("frame")
        plt.legend(iter(line), ('left2right', 'last2current',
                                'RANSAC Inliers'),  loc='best')

        pp.savefig(fig)
        pp.savefig(fig3)
        pp.close()


class storageRT:
    def __init__(self):
        self.odoEuler = []
        self.odoT = []
        self.optEuler = []
        self.optT = []
        self.kp = []
        self.Matches = []
        self.reprojErr = []

if __name__ == "__main__":
    outlierMask = []
    RT = storageRT()
    odoReader = odomLogReader(
        '/home/zhangtianyi/visual-odometry-custom/hawaii.csv', RT)
    plot3d = plot3d()
    fig3 = plot3d.getfigure()
    pdfname = '/home/zhangtianyi/MHLdata/hawaii.pdf'
    vis = visualizer(RT, fig3, pdfname, outlierMask)
