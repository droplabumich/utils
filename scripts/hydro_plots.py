#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import csv
from collections import defaultdict
import rosbag

from os.path import isfile, join
from os import listdir

def plot_xyz():
    time = [-1,-0.5,-0.2,-0.1,0.1,0.2,0.5,1]
    xu = [-3.896145, -0.999390, -0.168604, -0.043885, 0.040731, 0.153863, 0.943543, 3.7207488]
    yv = [-17.369538, -4.348361, -0.712968, -0.184087, 0.184768, 0.7164395, 4.364918, 17.245638]
    zw =  [-36.950503, -9.4131002, -1.560104, -0.403978, 0.367276, 1.393455, 8.243847, 32.353907]


    plt.plot(time, xu,'-*r', label='X_u', linewidth=2)
    plt.plot(time, yv, '-og', label='Y_v', linewidth=2)
    plt.plot(time, zw, '-^b', label='Z_w', linewidth=2)
    #plt.axis([63, 160, 0, 0.03])
    #plt.plot(column_pose, 'b', label='Pose depth')
    plt.legend(loc='upper left', fontsize = 20)
    plt.xlabel('Velocity (m/s)', size=24)
    plt.ylabel('Force (N)', size=24)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.tight_layout()
    #plt.title('Linear model hydrodynamic profile')
    plt.grid(True)
    plt.show()

    time_rot = [-1, -0.8, -0.5, -0.2, -0.1, 0.1, 0.2, 0.5, 0.8, 1]
    kp = [-0.159091, -0.159799, -0.13231044, -0.035165, -0.014577, 0.014825, 0.045770, 0.111738, 0.157376, 0.154321]
    mq = [-1.2767042, -1.1774095, -0.73856564, -0.23375, -0.073437639, 0.07551, 0.24218149, 0.78742706, 1.2643215, 1.3960098]
    nr = [-0.49682846, -0.4008394, -0.21114207, -0.049278559, -0.013846962, 0.013782303, 0.048156776, 0.19804741, 0.37075654, 0.45586599]

    plt.plot(time_rot, kp, '-*r', label='K_p', linewidth=2)
    plt.plot(time_rot, mq, '-og', label='M_q', linewidth=2)
    plt.plot(time_rot, nr, '-^b', label='N_r', linewidth=2)
    # plt.axis([63, 160, 0, 0.03])
    # plt.plot(column_pose, 'b', label='Pose depth')
    plt.legend(loc='upper left', fontsize = 20)
    plt.xlabel('Velocity (rad/s)', size=24)
    plt.ylabel('Moment (Nm)', size=24)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.tight_layout()

    # plt.title('Linear model hydrodynamic profile')
    plt.grid(True)
    plt.show()





if __name__=="__main__":
    plot_xyz()