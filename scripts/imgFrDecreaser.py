#!/usr/bin/env python

# this is the tool for decreasing img frequencies in bags

import time
import sys
from ros import rosbag
import roslib
from sensor_msgs.msg import Image
import datetime
from cv_bridge import CvBridge, CvBridgeError
import cv2
import rospy


def inject(inputBags, output_bag_path):
    output_bag = rosbag.Bag(output_bag_path, 'w')
    bridge = CvBridge()
    input_bag = rosbag.Bag(inputBags, 'r')
    count1 = 0
    count2 = 0
    for topic, msg, t in input_bag:
        if topic == '/cam0/image_raw':
            gray2 = bridge.imgmsg_to_cv2(msg, desired_encoding='8UC1')
            imgmsg = bridge.cv2_to_imgmsg(gray2, encoding='mono8')
            # decrease img frequency from 20hz to 10hz
            if count1 ==0:
                output_bag.write(topic, imgmsg, t)
            count1 += 1
            if count1 ==2:
                count1 = 0
        elif topic == '/cam1/image_raw':
            gray2 = bridge.imgmsg_to_cv2(msg, desired_encoding='8UC1')
            imgmsg = bridge.cv2_to_imgmsg(gray2, encoding='mono8')
            if count2 ==0:
                output_bag.write(topic, imgmsg, t)
            count2 += 1
            if count2 ==2:
                count2 = 0
        elif topic == '/imu0':
            output_bag.write(topic, msg, t)
    input_bag.close()
    output_bag.close()


def main():
    #define input bag and out put bag name
    output_bag_name = '/home/zhangtianyi/data/euroc/euroc10hz.bag'
    inputBag1 = '/home/zhangtianyi/data/euroc/MH_01_easy.bag'
    inject(inputBag1, output_bag_name)


if __name__ == "__main__":
    main()
