#!/usr/bin/env python
from __future__ import division
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.backends.backend_pdf import PdfPages
import csv
import rosbag
import numpy as np
import tf.transformations
import os

class SphereData():
    def __init__(self):
        self.altitude = []
        self.altitudeAverage = []
        self.atime = []
        self.depth = []
        self.ul = []
        self.ur = []
        self.fl = []
        self.fr = []
        self.lax = []
        self.lay = []
        self.laz = []
        self.roll = []
        self.pitch= []
        self.yaw = []
        self.full_list = []
        self.lltime = []
        self.temp_int_hih = []
        self.temp_int_mpl = []
        self.temp_int_xsens = []
        self.temp_ext = []
        self.press_int = []
        self.humidity = []
        self.current_3V = []
        self.current_5V = []
        self.current_bat = []
        self.rbat_voltage = []
        self.r5V_voltage = []
        self.r3V_voltage = []
        self.status = []
        self.user_iface = []


def bag_search(inputdirectory):
    bag=[]
    for file in os.listdir(inputdirectory):
        if file.endswith('.bag'):
            bag.append(inputdirectory+'/'+file)
    print(bag)
    return sorted(bag)

def average_of(lst):
    return sum(lst) / len(lst)

def read_bag(inputBagString):
    input_bag =  rosbag.Bag(inputBagString, 'r')

    spData = SphereData()

    rawdepth = []
    dtime = []
    rawul = []
    rawur = []
    rawfl = []
    rawfr = []
    rawlax = []
    rawlay =[]
    rawlaz = []
    itime = []
    ttime =[]
    rolll = []
    yawl =[]
    pitchl = []
    lltime = []
    ltemp_int_hih = []
    ltemp_int_mpl = []
    ltemp_int_xsens = []
    ltemp_ext = []
    lpress_int = []
    lhumidity = []
    lcurrent_3V = []
    lcurrent_5V = []
    lcurrent_bat = []
    lrbat_voltage = []
    lr5V_voltage = []
    lr3V_voltage = []
    lstatus = []
    luser_iface = []


    for topic, msg, t in input_bag.read_messages(topics=['/sphere_a/nav_sensors/altitude', '/sphere_a/nav_sensors/pressure_sensor', '/sphere_hw_iface/thruster_data', '/sphere_a/nav_sensors/imu', '/sphere_hw_iface/ll_data']):
            if topic=='/sphere_a/nav_sensors/altitude':
                if len(spData.altitudeAverage) < 10:
                    spData.altitudeAverage.append(msg.altitude)
                    spData.atime.append(round(msg.header.stamp.secs + msg.header.stamp.nsecs / 1000000000, 2))
                    spData.altitude.append(msg.altitude)
                else:
                    if (average_of(spData.altitudeAverage) * 5) > msg.altitude:
                        spData.altitude.append(msg.altitude)
                        spData.atime.append(round(msg.header.stamp.secs + msg.header.stamp.nsecs / 1000000000, 2))
                        del spData.altitudeAverage[0]
                        spData.altitudeAverage.append(msg.altitude)
            if topic == '/sphere_a/nav_sensors/pressure_sensor':
                rawdepth.append(round(msg.depth, 3))
                dtime.append(round(msg.header.stamp.secs + msg.header.stamp.nsecs / 1000000000, 2))
            if topic == '/sphere_hw_iface/thruster_data':
                rawul.append(msg.ul)
                rawur.append(msg.ur)
                rawfl.append(msg.fl)
                rawfr.append(msg.fr)
                ttime.append(round(msg.header.stamp.secs + msg.header.stamp.nsecs / 1000000000, 2))
            if topic == '/sphere_a/nav_sensors/imu':
                itime.append(round(msg.header.stamp.secs + msg.header.stamp.nsecs / 1000000000, 2))
                rawlax.append(msg.linear_acceleration.x)
                rawlay.append(msg.linear_acceleration.y)
                rawlaz.append(msg.linear_acceleration.z)
                quaternion = (
                    msg.orientation.x,
                    msg.orientation.y,
                    msg.orientation.z,
                    msg.orientation.w)
                euler = tf.transformations.euler_from_quaternion(quaternion)
                rawroll = euler[0]
                rawpitch = euler[1]
                rawyaw = euler[2]
                rolll.append(rawroll)
                pitchl.append(rawpitch)
                yawl.append(rawyaw)
            if topic == '/sphere_hw_iface/ll_data':
                lltime.append(round(msg.header.stamp.secs + msg.header.stamp.nsecs / 1000000000, 2))
                ltemp_int_hih.append(round(msg.temp_int_hih, 2))
                ltemp_int_mpl.append(round(msg.temp_int_mpl, 2))
                ltemp_int_xsens.append(round(msg.temp_int_xsens, 2))
                ltemp_ext.append(round(msg.temp_ext, 2))
                lpress_int.append(round(msg.press_int, 2))
                lhumidity.append(round(msg.humidity, 2))
                lcurrent_3V.append(round(msg.current_3V, 2))
                lcurrent_5V.append(round(msg.current_5V, 2))
                lcurrent_bat.append(round(msg.current_bat, 2))
                lrbat_voltage.append(round(msg.rbat_voltage, 2))
                lr5V_voltage.append(round(msg.r5V_voltage, 2))
                lr3V_voltage.append(round(msg.r3V_voltage, 2))
                lstatus.append(round(msg.status, 2))
                luser_iface.append(round(msg.user_iface, 2))




    spData.depth = list(np.interp(spData.atime, dtime, rawdepth))
    spData.ul = list(np.interp(spData.atime, ttime, rawul))
    spData.ur = list(np.interp(spData.atime, ttime, rawur))
    spData.fl = list(np.interp(spData.atime, ttime, rawfl))
    spData.fr = list(np.interp(spData.atime, ttime, rawfr))
    spData.roll = list(np.interp(spData.atime, itime, rolll))
    spData.pitch = list(np.interp(spData.atime, itime, pitchl))
    spData.yaw = list(np.interp(spData.atime, itime, yawl))
    spData.lax = list(np.interp(spData.atime, itime, rawlax))
    spData.lay = list(np.interp(spData.atime, itime, rawlay))
    spData.laz = list(np.interp(spData.atime, itime, rawlaz))
    spData.temp_int_hih = list(np.interp(spData.atime, lltime, ltemp_int_hih))
    spData.temp_int_mpl = list(np.interp(spData.atime, lltime, ltemp_int_mpl))
    spData.temp_int_xsens = list(np.interp(spData.atime, lltime, ltemp_int_xsens))
    spData.temp_ext = list(np.interp(spData.atime, lltime, ltemp_ext))
    spData.press_int = list(np.interp(spData.atime, lltime, lpress_int))
    spData.humidity = list(np.interp(spData.atime, lltime, lhumidity))
    spData.current_3V = list(np.interp(spData.atime, lltime, lcurrent_3V))
    spData.current_5V = list(np.interp(spData.atime, lltime, lcurrent_5V))
    spData.current_bat = list(np.interp(spData.atime, lltime, lcurrent_bat))
    spData.rbat_voltage = list(np.interp(spData.atime, lltime, lrbat_voltage))
    spData.r5V_voltage= list(np.interp(spData.atime, lltime, lr5V_voltage))
    spData.r3V_voltage= list(np.interp(spData.atime, lltime, lr3V_voltage))
    spData.status = list(np.interp(spData.atime, lltime, lstatus))
    spData.user_iface= list(np.interp(spData.atime, lltime, luser_iface))

    return spData


def plot_data(data):
    pp = PdfPages('/home/dropshere/Data/Hawaii2019/20191010/2013_01_01_000734/multipage.pdf')
    globalData = SphereData()
    for entry in data:
        globalData.atime = globalData.atime + entry.atime
        globalData.altitude = globalData.altitude + entry.altitude
        globalData.depth = globalData.depth + entry.depth
        globalData.ul = globalData.ul + entry.ul
        globalData.ur = globalData.ur + entry.ur
        globalData.fl = globalData.fl + entry.fl
        globalData.fr = globalData.fr + entry.fr
        globalData.roll = globalData.roll + entry.roll
        globalData.pitch = globalData.pitch + entry.pitch
        globalData.yaw = globalData.yaw + entry.yaw
        globalData.lax = globalData.lax + entry.lax
        globalData.lay = globalData.lay + entry.lay
        globalData.laz = globalData.laz + entry.laz
        globalData.temp_int_hih = globalData.temp_int_hih + entry.temp_int_hih
        globalData.temp_int_mpl = globalData.temp_int_mpl + entry.temp_int_mpl
        globalData.temp_int_xsens = globalData.temp_int_xsens + entry.temp_int_xsens
        globalData.temp_ext = globalData.temp_ext + entry.temp_ext
        globalData.press_int = globalData.press_int + entry.press_int
        globalData.humidity = globalData.humidity + entry.humidity
        globalData.current_3V = globalData.current_3V + entry.current_3V
        globalData.current_5V = globalData.current_5V + entry.current_5V
        globalData.current_bat = globalData.current_bat + entry.current_bat
        globalData.rbat_voltage = globalData.rbat_voltage + entry.rbat_voltage
        globalData.r5V_voltage = globalData.r5V_voltage + entry.r5V_voltage
        globalData.r3V_voltage = globalData.r3V_voltage + entry.r3V_voltage
        globalData.status = globalData.status + entry.status
        globalData.user_iface = globalData.user_iface + entry.user_iface

        newRoll = [i * (180/np.pi) for i in globalData.roll]
        newPitch = [i * (180 / np.pi) for i in globalData.pitch]
        newYaw = [i * (180 / np.pi) for i in globalData.yaw]

    globalData.atime = [x - globalData.atime[0] for x in globalData.atime]
    Altitude = plt.figure(1, figsize=(7, 5))
    plt.ylabel('Distance[m]', fontsize=24)
    plt.xlabel('Time[s]', fontsize=20)
    plt.title('Altitude, Depth, Total Depth', fontsize=24)
    red_patch = mpatches.Patch(color='red', label='Altitude')
    blue_patch = mpatches.Patch(color='blue', label='Depth')
    green_patch = mpatches.Patch(color='green', label='Total Depth')
    plt.legend(handles=[red_patch, blue_patch, green_patch])
    plt.plot(globalData.atime, globalData.altitude, 'r-')
    plt.plot(globalData.atime, globalData.depth, 'b-')
    plt.plot(globalData.atime, map(sum, zip(globalData.altitude, globalData.depth)), 'g-')
    Thrusters, axs = plt.subplots(4, 1, sharex=True, figsize =(7,5))
    Thrusters.subplots_adjust(hspace=.1)
    plt.xlabel('Time[s]', fontsize=20)
    axs[2].set_ylabel('Thruster Values', fontsize=24)
    axs[0].set_title('Thruster Data', fontsize=24)
    red_patch = mpatches.Patch(color='red', label='Upper Left')
    blue_patch = mpatches.Patch(color='blue', label='Upper Right')
    green_patch = mpatches.Patch(color='green', label='Front Left')
    yellow_patch = mpatches.Patch(color='yellow', label='Front Right')
    plt.legend(bbox_to_anchor=(1.15, 4.5), handles=[red_patch, blue_patch, green_patch, yellow_patch])
    axs[0].plot(globalData.atime, globalData.ul, 'r-')
    axs[1].plot(globalData.atime, globalData.ur, 'b-')
    axs[2].plot(globalData.atime, globalData.fl, 'g-')
    axs[3].plot(globalData.atime, globalData.fr, 'y-')
    RollPitch = plt.figure(4, figsize=(7, 5))
    plt.xlabel('Time[s]', fontsize=20)
    plt.ylabel('Orientation[degrees]', fontsize=24)
    plt.title('Roll, Pitch', fontsize=24)
    red_patch = mpatches.Patch(color='red', label='Roll')
    blue_patch = mpatches.Patch(color='blue', label='Pitch')
    plt.legend(handles=[red_patch, blue_patch])
    plt.plot(globalData.atime, newRoll, 'r-')
    plt.plot(globalData.atime, newPitch, 'b-')
    Yaw = plt.figure(5, figsize=(7, 5))
    plt.xlabel('Time[s]', fontsize=20)
    plt.ylabel('Orientation[degrees]', fontsize=24)
    plt.title('Yaw', fontsize=24)
    green_patch = mpatches.Patch(color='green', label='Yaw')
    plt.legend(handles=[green_patch])
    plt.plot(globalData.atime, newYaw, 'g-')
    LA = plt.figure(6, figsize=(7, 5))
    plt.xlabel('Time[s]', fontsize=20)
    plt.ylabel('Linear Acceleration[m/s^2]', fontsize=24)
    plt.title('Linear Acceleration', fontsize=24)
    red_patch = mpatches.Patch(color='red', label='X')
    blue_patch = mpatches.Patch(color='blue', label='Y')
    green_patch = mpatches.Patch(color='green', label='Z')
    plt.legend(bbox_to_anchor=(1.15, 1.1), handles=[red_patch, blue_patch, green_patch])
    plt.plot(globalData.atime, globalData.lax, 'r-')
    plt.plot(globalData.atime, globalData.lay, 'b-')
    plt.plot(globalData.atime, globalData.laz, 'g-')
    temps = plt.figure(7, figsize=(7, 5))
    plt.ylabel('Temperature[C]', fontsize=24)
    plt.xlabel('Time[s]', fontsize=20)
    plt.title('Temperature Int hih, Int mpl, Int xsens, Ext', fontsize=24)
    red_patch = mpatches.Patch(color='red', label='Int Temp hih')
    blue_patch = mpatches.Patch(color='blue', label='Int Temp mpl')
    green_patch = mpatches.Patch(color='green', label='Int Temp xsens')
    yellow_patch = mpatches.Patch(color='yellow', label='Ext Temp')
    plt.legend(handles=[red_patch, blue_patch, green_patch, yellow_patch])
    plt.plot(globalData.atime, globalData.temp_int_hih, 'r-')
    plt.plot(globalData.atime, globalData.temp_int_mpl, 'b-')
    plt.plot(globalData.atime, globalData.temp_int_xsens, 'g-')
    plt.plot(globalData.atime, globalData.temp_ext, 'y-')
    press_int = plt.figure(11, figsize=(7, 5))
    plt.ylabel('Internal Pressure[Pa]', fontsize=24)
    plt.xlabel('Time[s]', fontsize=20)
    plt.title('Internal Pressure', fontsize=24)
    red_patch = mpatches.Patch(color='red', label='Internal Pressure')
    plt.legend(handles=[red_patch])
    plt.plot(globalData.atime, globalData.press_int, 'r-')
    humidity = plt.figure(12, figsize=(7, 5))
    plt.ylabel('Humidity[%]', fontsize=24)
    plt.xlabel('Time[s]', fontsize=20)
    plt.title('Humidity', fontsize=24)
    red_patch = mpatches.Patch(color='red', label='Humidity')
    plt.legend(handles=[red_patch])
    plt.plot(globalData.atime, globalData.humidity, 'r-')
    currents = plt.figure(13, figsize=(7, 5))
    plt.ylabel('Current[A]', fontsize=24)
    plt.xlabel('Time[s]', fontsize=20)
    plt.title('3V Current, 5V Current, Bat Current', fontsize=24)
    red_patch = mpatches.Patch(color='red', label='3V Current')
    blue_patch = mpatches.Patch(color='blue', label='5V Current')
    green_patch = mpatches.Patch(color='green', label='Bat Current')
    plt.legend(handles=[red_patch, blue_patch, green_patch])
    plt.plot(globalData.atime, globalData.current_3V, 'r-')
    plt.plot(globalData.atime, globalData.current_5V, 'b-')
    plt.plot(globalData.atime, globalData.current_bat, 'g-')
    voltages = plt.figure(16, figsize=(7, 5))
    plt.ylabel('Voltage[V]', fontsize=24)
    plt.xlabel('Time[s]', fontsize=20)
    plt.title('rBat, r5V, r3V Voltage', fontsize=24)
    red_patch = mpatches.Patch(color='red', label='rBat')
    blue_patch = mpatches.Patch(color='blue', label='r5V')
    green_patch = mpatches.Patch(color='green', label='r3V')
    plt.legend(handles=[red_patch, blue_patch, green_patch])
    plt.plot(globalData.atime, globalData.rbat_voltage, 'r-')
    plt.plot(globalData.atime, globalData.r5V_voltage, 'b-')
    plt.plot(globalData.atime, globalData.r3V_voltage, 'g-')
    status = plt.figure(19, figsize=(7, 5))
    plt.ylabel('Status', fontsize=24)
    plt.xlabel('Time[s]', fontsize=20)
    plt.title('Status', fontsize=24)
    red_patch = mpatches.Patch(color='red', label='Status')
    plt.legend(handles=[red_patch])
    plt.plot(globalData.atime, globalData.status, 'r-')
    user_iface = plt.figure(20, figsize=(7, 5))
    plt.ylabel('User Interface', fontsize=24)
    plt.xlabel('Time[s]', fontsize=20)
    plt.title('User Interface', fontsize=24)
    red_patch = mpatches.Patch(color='red', label='User Interface')
    plt.legend(handles=[red_patch])
    plt.plot(globalData.atime, globalData.altitude, 'r-')
    plt.tight_layout()
    plt.show()
    pp.savefig(Thrusters)
    pp.savefig(Altitude)
    pp.savefig(RollPitch)
    pp.savefig(Yaw)
    pp.savefig(LA)
    pp.savefig(temps)
    pp.savefig(press_int)
    pp.savefig(humidity)
    pp.savefig(currents)
    pp.savefig(voltages)
    pp.savefig(status)
    pp.savefig(user_iface)
    pp.close()

def clear_csv(output_path):
    os.remove(output_path+'/csv_file.csv')

def header_csv(output_path):
    writer = csv.writer(open(output_path + '/csv_file.csv', 'w'))
    writer.writerow(["Time[s]", "Altitude[m]", "Depth[m]", "ULThrust", "URThrust", "FLThrust", "FRThrust", "Roll[degrees]",
         "Pitch[degrees]", "Yaw[degrees]", "X Linear Acceleration[m/s^2]", "Y Linear Acceleration[m/s^2]",
         "Z Linear Acceleration[m/s^2]", "Int Temp hih[C]", "Int Temp mpl[C]", "Int Temp xsens[C]", "Ext Temp[C]", "Int Press[Pa]", "Humidity[%]", "3V current[a]", "5V current[a]", "Bat current[a]", "rBat voltage[v]", "r5V voltage[v]", "r3V voltage[v]", "status", "user_iface"])

def write_csv(sphereVariables, output_path):
    writer = csv.writer(open(output_path+'/csv_file.csv', 'a'))
    for i in range(len(sphereVariables.altitude)):
            altitudepos = sphereVariables.altitude[i]
            depthpos = round(sphereVariables.depth[i], 3)
            atimepos = sphereVariables.atime[i]
            ulpos = round(sphereVariables.ul[i], 3)
            urpos = round(sphereVariables.ur[i], 3)
            flpos = round(sphereVariables.fl[i], 3)
            frpos = round(sphereVariables.fr[i], 3)
            rollpos = round((180/np.pi)*(sphereVariables.roll[i]), 1)
            pitchpos = round((180/np.pi)*(sphereVariables.pitch[i]), 1)
            yawpos = round((180/np.pi)*(sphereVariables.yaw[i]), 1)
            laxpos = round(sphereVariables.lax[i], 3)
            laypos = round(sphereVariables.lay[i], 3)
            lazpos = round(sphereVariables.laz[i], 3)
            temp_int_hihpos = round(sphereVariables.temp_int_hih[i], 3)
            temp_int_mplpos = round(sphereVariables.temp_int_mpl[i], 3)
            temp_int_xsenspos = round(sphereVariables.temp_int_xsens[i], 3)
            temp_extpos = round(sphereVariables.temp_ext[i], 3)
            press_intpos = round(sphereVariables.press_int[i], 3)
            humiditypos = round(sphereVariables.humidity[i], 3)
            current_3Vpos = round(sphereVariables.current_3V[i], 3)
            current_5Vpos = round(sphereVariables.current_5V[i], 3)
            current_batpos = round(sphereVariables.current_bat[i], 3)
            rbat_voltagepos = round(sphereVariables.rbat_voltage[i], 3)
            r5V_voltagepos = round(sphereVariables.r5V_voltage[i], 3)
            r3V_voltagepos = round(sphereVariables.r3V_voltage[i], 3)
            statuspos = round(sphereVariables.status[i], 3)
            user_ifacepos = round(sphereVariables.user_iface[i], 3)

            row = [atimepos, altitudepos, depthpos, ulpos, urpos, flpos, frpos, rollpos, pitchpos, yawpos, laxpos, laypos, lazpos,
                   temp_int_hihpos, temp_int_mplpos, temp_int_xsenspos, temp_extpos, press_intpos, humiditypos, current_3Vpos,
                   current_5Vpos, current_batpos, rbat_voltagepos, r5V_voltagepos, r3V_voltagepos, statuspos, user_ifacepos]
            writer.writerow(row)






if __name__=="__main__":
    inputpath = '/home/dropshere/Data/Hawaii2019/20191010/2013_01_01_000734/' #'/home/dropshere/Data/Hawaii2019/20191009/ros_logs/2013_01_01_000221/' ''/home/dropshere/Data/Hawaii2019/20191008/2013_01_01_011932''
    bag_list = bag_search(inputpath)
    data = []
    #clear_csv(inputpath)
    header_csv(inputpath)

    for i in range(0, len(bag_list)):
        data.append(read_bag(bag_list[i]))
        write_csv(data[i], inputpath)

    plot_data(data)
