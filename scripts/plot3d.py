#!/usr/bin/python

import csv
import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from icp import *


class plot3d:
    def __init__(self, mask):

        R = []
        T = []

        with open("/home/zhangtianyi/visual-odometry-custom/kf_2018_04_17_222408.csv") as csv_file:
            csv_reader = csv.reader(csv_file)
            line_count = 0
            R.append(np.eye(3))
            for row in csv_reader:
                if line_count > 4:
                    R.append(np.array(map(float, row[0:9])).reshape((3, 3)))
                    T.append(np.array(map(float, row[9:12])))
                line_count += 1

        R_global = np.array(R)
        T_global = np.array(T)

        # print np.shape(R_global)
        # print np.shape(T_global)

        for i in range(1, line_count-5):
            R_global[i] = np.matmul(R_global[i, :, :], R_global[i-1, :, :])
            T_global[i, :] = T_global[i-1, :] + \
                np.matmul(R_global[i-1, :, :], T_global[i, :])

        pos3d = []

        with open("/home/zhangtianyi/MHLdata/2018_04_17_222408/log1.csv") as csv_file:
            csv_reader = csv.reader(csv_file)
            line_count = 0
            for row in csv_reader:
                if line_count > 1:
                    r = np.array(map(float, row[5:8]))
                    pos3d.append(r)
                line_count += 1

        with open("/home/zhangtianyi/MHLdata/2018_04_17_222408/log2.csv") as csv_file:
            csv_reader = csv.reader(csv_file)
            line_count2 = 0
            for row in csv_reader:
                if line_count2 > 0:
                    r = np.array(map(float, row[5:8]))
                    pos3d.append(r)
                line_count2 += 1
                line_count += 1

        pos3darray = np.array(pos3d)
        pos3darray = (pos3darray-pos3darray[0, :])*0.3048
        pos3darray[:, 0] = pos3darray[:, 0]
        pos3darray[:, 1] = pos3darray[:, 1]
        # ax.plot(pos3darray[:, 0], pos3darray[:, 1], pos3darray[:, 2])
        # ax.set_xlabel("x(m)")
        # ax.set_ylabel("y(m)")
        # ax.set_zlabel("depth(m)")
        # ax.set_zlim(-3, 3)
        ###############################################################
        icper = icp()
        tempsrc, generalR = icper.runIcp(T_global[:, :], pos3darray, 2)
        # ax.plot(tempsrc[:, 0], tempsrc[:, 1], tempsrc[:, 2])
        
        odotjl = []
        # odotj = tempsrc
        # print len(tempsrc)
        # print len(mask)
        for i in range(0,54):
            # if mask[i] == 1:
                odotjl.append(tempsrc[i])

        odotj = np.array(odotjl)
        # print odotj
        self.fig = plt.figure(figsize=(14,12))

        ax = self.fig.add_subplot(2,2,1,projection='3d')
        line1, = ax.plot(pos3darray[:,0], pos3darray[:,1], pos3darray[:,2])
        line2, = ax.plot(odotj[:,0], odotj[:,1], odotj[:,2])
        ax.view_init(10,45)
        ax.set_zlim(-3,3)
        ax.set_xlabel("x(m)")
        ax.set_ylabel("y(m)")
        ax.set_zlabel("depth(m)")
        ax.axis('equal')
        line1.set_label('Optitrack')
        line2.set_label('Visual_Odometry')
        ax.legend(loc='best')

        ax = self.fig.add_subplot(2,2,2,projection='3d')
        line1, = ax.plot(pos3darray[:,0], pos3darray[:,1], pos3darray[:,2])
        line2, = ax.plot(odotj[:,0], odotj[:,1], odotj[:,2])
        ax.view_init(15,135)
        ax.set_zlim(-3,3)
        ax.set_xlabel("x(m)")
        ax.set_ylabel("y(m)")
        ax.set_zlabel("depth(m)")
        ax.axis('equal')
        line1.set_label('Optitrack')
        line2.set_label('Visual_Odometry')
        ax.legend(loc='best')

        ax = self.fig.add_subplot(2,2,3,projection='3d')
        line1, = ax.plot(pos3darray[:,0], pos3darray[:,1], pos3darray[:,2])
        line2, = ax.plot(odotj[:,0], odotj[:,1], odotj[:,2])
        ax.view_init(35,225)
        ax.set_zlim(-3,3)
        ax.set_xlabel("x(m)")
        ax.set_ylabel("y(m)")
        ax.set_zlabel("depth(m)")
        ax.axis('equal')
        line1.set_label('Optitrack')
        line2.set_label('Visual_Odometry')
        ax.legend(loc='best')

        ax = self.fig.add_subplot(2,2,4,projection='3d')
        line1, = ax.plot(pos3darray[:,0], pos3darray[:,1], pos3darray[:,2])
        line2, = ax.plot(odotj[:,0], odotj[:,1], odotj[:,2])
        ax.view_init(50,315)
        ax.set_zlim(-3,3)
        ax.set_xlabel("x(m)")
        ax.set_ylabel("y(m)")
        ax.set_zlabel("depth(m)")
        ax.axis('equal')
        line1.set_label('Optitrack')
        line2.set_label('Visual_Odometry')
        ax.legend(loc='best')
        # print("plot3d done")
        # plt.show()

    def getfigure(self):
        return self.fig
