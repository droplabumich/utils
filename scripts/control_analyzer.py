#!/usr/bin/python

# Start up ROS pieces.
from __future__ import print_function

import matplotlib.pyplot as plt
import rosbag
import os
import tf2_ros
import tf2_geometry_msgs
from tf import transformations as tfr
import argparse
import numpy as np
from geometry_msgs.msg import PoseStamped
from utils.msg import LowLevelData

class BagReporter():

    def __init__(self, filename, splitbags=False, output=None):

        if not splitbags:
            # If a single bagfile is passed as argument, convert it to a list
            filename = [filename]


        self.bag_dir = os.path.dirname(filename[0])
        self.bag_name_we = os.path.basename(filename[0])

        self.bag_name= self.parseBagName(self.bag_name_we)

        # Choose output directory
        if output is None:
            self.output_dir = os.path.join(self.bag_dir, self.bag_name+'_report/')
        else:
            if not os.path.isdir(output):
                print("Output must be a path")
                exit(-1)
            self.output_dir = output

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

        imu_topic = '/sphere_a/nav_sensors/imu'
        setpoint_topic = '/pilot/position_req'
        thruster_topic = '/sphere_hw_iface/thruster_data'
        depth_topic = '/sphere_a/nav_sensors/pressure_sensor'
        ll_data_topic = '/sphere_hw_iface/ll_data'


        self.imu_timebase = []
        self.roll = []
        self.pitch = []
        self.yaw = []

        self.x = []
        self.y = []
        self.z = []
        self.pressure_timebase = []

        self.fl = []
        self.fr = []
        self.ur = []
        self.ul = []
        self.thruster_timebase = []

        self.setpoint_x = []
        self.setpoint_y = []
        self.setpoint_z = []
        self.setpoint_roll = []
        self.setpoint_pitch = []
        self.setpoint_yaw = []
        self.setpoint_timebase = []
        self.first_setpoint = True

        self.ll_roll = []
        self.ll_pitch = []
        self.ll_yaw = []
        self.ll_vol5V = []
        self.ll_vol3V3 = []
        self.ll_volBat = []
        self.ll_curr5V = []
        self.ll_curr3V3 = []
        self.ll_currBat = []
        self.ll_data_timebase = []

        # Open bag file.
        self.buffer = tf2_ros.Buffer(None, False)

        for bagfile in filename:
            bag = rosbag.Bag(bagfile, 'r')
            self.imu_count = bag.get_message_count([imu_topic])
            self.setpoint_count = bag.get_message_count([setpoint_topic])

            for topic, msg, t in bag.read_messages():
                if topic == imu_topic :
                    self.imu_callback(msg)
                if topic == setpoint_topic:
                    self.setpoint_callback(msg, t)
                if topic == thruster_topic:
                    self.thruster_callback(msg, t)
                if topic == depth_topic:
                    self.pressure_callback(msg, t)
                if topic == ll_data_topic:
                    self.lldata_callback(msg, t)
                if topic == "/tf_static":
                    for transform in msg.transforms:
                        self.buffer.set_transform(transform, "default_authority")

        #self.compute_control_stats(self.setpoint_z,self.setpoint_timebase, self.z, self.pressure_timebase)
        self.shift_timebase()
        self.plot_energy()

    def shift_timebase(self):
        self.imu_timebase = np.asarray(self.imu_timebase)
        self.setpoint_timebase = np.asarray(self.setpoint_timebase)
        self.thruster_timebase = np.asarray(self.thruster_timebase)
        self.ll_data_timebase = np.asarray(self.ll_data_timebase)
        self.pressure_timebase = np.asarray(self.pressure_timebase)

        time_zero = min(np.amin(self.imu_timebase), np.amin(self.thruster_timebase),
                        np.amin(self.ll_data_timebase), np.amin(self.pressure_timebase))

        self.imu_timebase -= time_zero
        self.setpoint_timebase -= time_zero
        self.thruster_timebase -= time_zero
        self.ll_data_timebase -= time_zero
        self.pressure_timebase -= time_zero

        pass

    def lldata_callback(self, msg, t):
        self.ll_roll.append(msg.roll)
        self.ll_pitch.append(msg.pitch)
        self.ll_yaw.append(msg.yaw)
        self.ll_vol5V.append(msg.r5V_voltage)
        self.ll_vol3V3.append(msg.r3V_voltage)
        self.ll_volBat.append(msg.rbat_voltage)
        self.ll_curr5V.append(msg.current_5V)
        self.ll_curr3V3.append(msg.current_3V)
        self.ll_currBat.append(msg.current_bat)
        self.ll_data_timebase.append(t.to_sec())

    def imu_callback(self,msg):
        #Transform to coordinate frame
        #transform = self.buffer.lookup_transform('sphere_a_ned', 'sphere_a/imu_frame', rospy.Time(0))
        transform = tf2_ros.TransformStamped()

        test_quat = tfr.quaternion_from_euler(0, 0, np.pi/2,'rxyz')
        test_quat_inv = tfr.quaternion_inverse(test_quat)
        imu_quat = [msg.orientation.x,
                    msg.orientation.y,
                    msg.orientation.z,
                    msg.orientation.w]
        out_quat = tfr.quaternion_multiply(test_quat,imu_quat)
        out_quat = tfr.quaternion_multiply(out_quat, test_quat_inv)
        out_euler = tfr.euler_from_quaternion(out_quat)

        imu_r, imu_p, imu_y = tfr.euler_from_quaternion([msg.orientation.x,
                                                 msg.orientation.y,
                                                 msg.orientation.z,
                                                 msg.orientation.w])
        col = np.array([[imu_r, imu_p, imu_y]]).T


        outMat = np.dot(self.rot_yaw(np.pi/2), col)


        transform.transform.rotation.x = test_quat[0]
        transform.transform.rotation.y = test_quat[1]
        transform.transform.rotation.z = test_quat[2]
        transform.transform.rotation.w = test_quat[3]
        transform.child_frame_id = "sphere_a/imu_frame"
        transform.header.frame_id = "sphere_a_ned"
        #print(tfr.euler_from_quaternion([transform.transform.rotation.x,
        #                                                transform.transform.rotation.y,
        #                                                transform.transform.rotation.z,
        #                                                transform.transform.rotation.w]))

        pose = PoseStamped()
        pose.header = msg.header
        pose.pose.orientation = msg.orientation
        out = tf2_geometry_msgs.do_transform_pose(pose,transform)

        msg.orientation = out.pose.orientation
        # Convert to RPY
        (r, p, y) = tfr.euler_from_quaternion([msg.orientation.x,
                                               msg.orientation.y,
                                               msg.orientation.z,
                                               msg.orientation.w])

        self.roll.append(np.rad2deg(imu_r))
        self.pitch.append(np.rad2deg(imu_p))
        self.yaw.append(np.rad2deg(imu_y))

        #self.roll.append(np.rad2deg(outMat[0]))
        #self.pitch.append(np.rad2deg(outMat[1]))
        #self.yaw.append(np.rad2deg(outMat[2]))

        self.imu_timebase.append(msg.header.stamp.to_sec())

    def rot_mat(self,phi,theta,psi):
        J = np.zeros((3,3))
        J[0, 0] = np.cos(theta) * np.cos(psi)
        J[0, 1] = np.cos(psi) * np.sin(theta) * np.sin(phi) - np.sin(psi) * np.cos(phi)
        J[0, 2] = np.sin(psi) * np.sin(phi) + np.cos(psi) * np.cos(phi) * np.sin(theta)

        J[1, 0] = np.cos(theta) * np.sin(psi)
        J[1, 1] = np.cos(psi) * np.cos(phi) + np.sin(phi) * np.sin(theta) * np.sin(psi)
        J[1, 2] = np.sin(psi) * np.sin(theta) * np.cos(phi) - np.cos(psi) * np.sin(phi)

        J[2, 0] = -np.sin(theta)
        J[2, 1] = np.cos(theta) * np.sin(phi)
        J[2, 2] = np.cos(theta) * np.cos(phi)
        return J

    def rot_yaw(self,yaw):
        J = np.zeros((3,3))
        J[0,0] = np.cos(yaw)
        J[0,1] = np.sin(yaw)
        J[1,0] = -np.sin(yaw)
        J[1,1] = np.cos(yaw)
        J[2,2] = 1
        return J

    def compute_control_stats(self,setpoint,setpoint_time,values,values_time):

        interp_setpoints = np.interp(values_time, setpoint_time, setpoint)

        error = values - interp_setpoints

        avg_error = np.mean(error)

        print(avg_error)

    def setpoint_callback(self, msg, timestamp):
        print("Got setpoint")
        if not self.first_setpoint:
            self.setpoint_x.append(self.setpoint_x[-1])
            self.setpoint_y.append(self.setpoint_y[-1])
            self.setpoint_z.append(self.setpoint_z[-1])

            self.setpoint_roll.append(self.setpoint_roll[-1])
            self.setpoint_pitch.append(self.setpoint_pitch[-1])
            self.setpoint_yaw.append(self.setpoint_yaw[-1])

            self.setpoint_timebase.append(timestamp.to_sec())

        self.setpoint_x.append(msg.position[0])
        self.setpoint_y.append(msg.position[1])
        self.setpoint_z.append(msg.position[2])

        self.setpoint_roll.append(np.rad2deg(msg.position[3]))
        self.setpoint_pitch.append(np.rad2deg(msg.position[4]))
        self.setpoint_yaw.append(np.rad2deg(msg.position[5]))

        self.setpoint_timebase.append(timestamp.to_sec())
        self.first_setpoint = False

    def thruster_callback(self, msg, timestamp):
        self.ul.append(msg.ul)
        self.ur.append(msg.ur)
        self.fl.append(msg.fl)
        self.fr.append(msg.fr)

        self.thruster_timebase.append(timestamp.to_sec())

    def pressure_callback(self, msg, timestamp):

        self.z.append(msg.depth)
        self.pressure_timebase.append(msg.header.stamp.to_sec())

    def plot_pose(self):

        fig_orientation, (ax_or0, ax_or1, ax_or2, ax_or3) = plt.subplots(nrows=4, sharex=True)
        ax_or0.set_title('Roll')
        ax_or1.set_title('Pitch ')
        ax_or2.set_title('Yaw')

        colors = iter(plt.cm.rainbow(np.linspace(0, 1, 4)))
        ul_color = next(colors)
        ur_color = next(colors)
        fl_color = next(colors)
        fr_color = next(colors)

        ax_or0.plot(self.imu_timebase, self.roll, '--', c='r')
        ax_or0.plot(self.ll_data_timebase, self.ll_roll, '-', c='r')
        ax_or1.plot(self.imu_timebase, self.pitch, '--',c='g')
        ax_or1.plot(self.ll_data_timebase, self.ll_pitch, '-', c='g')
        ax_or2.plot(self.imu_timebase, self.yaw, '--', c='b')
        ax_or2.plot(self.ll_data_timebase, self.ll_yaw, '-', c='b')

        ax_or0.plot(self.setpoint_timebase, self.setpoint_roll, '-',c='r')
        ax_or1.plot(self.setpoint_timebase, self.setpoint_pitch, '-',c='g')
        ax_or2.plot(self.setpoint_timebase, self.setpoint_yaw, '-', c='b')
        ax_or3.plot(self.thruster_timebase, self.ul, '-', c=ul_color)
        ax_or3.plot(self.thruster_timebase, self.ur, 'x', c=ur_color)
        ax_or3.plot(self.thruster_timebase, self.fl, 'o', c=fl_color)
        ax_or3.plot(self.thruster_timebase, self.fr, c=fr_color)

        ax_or0.legend(['Roll','Raw'], loc='upper left')
        ax_or1.legend(['Pitch','Raw'], loc='upper left')
        ax_or2.legend(['Yaw','Raw'], loc='upper left')
        ax_or3.legend(['UL', 'UR', 'FL', 'FR'], loc='upper left')

        fig_pos, (ax_pos0, ax_pos1, ax_pos2, ax_pos3) = plt.subplots(nrows=4)

        ax_pos0.set_title('X')
        ax_pos1.set_title('Y ')
        ax_pos2.set_title('Z')

        ax_pos2.plot(self.pressure_timebase, self.z, '--', c='b')
        ax_pos2.plot(self.setpoint_timebase, self.setpoint_z, '-', c='b')

        ax_pos3.plot(self.thruster_timebase, self.ul, '-', c=ul_color)
        ax_pos3.plot(self.thruster_timebase, self.ur, 'x', c=ur_color)
        ax_pos3.plot(self.thruster_timebase, self.fl, 'o', c=fl_color)
        ax_pos3.plot(self.thruster_timebase, self.fr, c=fr_color)

        ax_pos2.legend(["Depth Sensor"])
        ax_pos3.legend(['UL', 'UR', 'FL', 'FR'], loc='upper left')
        plt.tight_layout()

        plt.show()

    def plot_energy(self):
        fig_energy, (ax_en0, ax_en1, ax_en2, ax_en3) = plt.subplots(nrows=4, sharex=True)
        ax_en0.set_title('5V Rail')
        ax_en1.set_title('3V3 Rail ')
        ax_en2.set_title('Battery Rail')
        ax_en3.set_title('Thruster Setpoints')

        ax_en0.plot(self.ll_data_timebase, self.ll_vol5V, '--', c='r')
        ax_en0_twin = ax_en0.twinx()
        ax_en0.set_ylabel('Voltage [V]', color='r')
        ax_en0_twin.set_ylabel('Current [mA]', color='g')
        ax_en0_twin.plot(self.ll_data_timebase, self.ll_curr5V, '-', c='g')
        ax_en1.plot(self.ll_data_timebase, self.ll_vol3V3, '--', c='r')
        ax_en1_twin = ax_en1.twinx()
        ax_en1_twin.set_ylabel('Current [mA]', color='g')
        ax_en1.set_ylabel('Voltage [V]', color='r')

        ax_en1_twin.plot(self.ll_data_timebase, self.ll_curr3V3, '-', c='g')
        ax_en2.plot(self.ll_data_timebase, self.ll_volBat, '--', c='r')
        ax_en2_twin = ax_en2.twinx()
        ax_en2.set_ylabel('Voltage [V]', color='r')

        ax_en2_twin.plot(self.ll_data_timebase, self.ll_currBat, '-', c='g')
        ax_en2_twin.set_ylabel('Current [mA]', color='g')
        colors = iter(plt.cm.rainbow(np.linspace(0, 1, 4)))
        ul_color = next(colors)
        ur_color = next(colors)
        fl_color = next(colors)
        fr_color = next(colors)
        ax_en3.plot(self.thruster_timebase, self.ul, '-', c=ul_color)
        ax_en3.plot(self.thruster_timebase, self.ur, 'x', c=ur_color)
        ax_en3.plot(self.thruster_timebase, self.fl, 'o', c=fl_color)
        ax_en3.plot(self.thruster_timebase, self.fr, c=fr_color)
        ax_en3.set_xlabel("Time [sec]")
        plt.tight_layout()

        plt.show()



    def parseBagName(self, bagname):
        # Get bag group name
        try:
            bagname = bagname.split('_')[-2]
        except:
            bagname = os.path.splitext(self.bag_name_we)[0]
        return bagname


def getBagFiles(directory,filetype):
    if os.path.isfile(directory):
        return [directory]

    templist = os.listdir(directory)
    filelist = []
    #print "Templist in "+str(directory)+" folder: "+str(templist)
    for name in templist:
        if os.path.isfile(os.path.join(directory, name)):
            if os.path.splitext(name)[1] in filetype:
                filelist.append(os.path.join(directory, name))
            else:
                pass
                #print("Not supported format: " + str(os.path.splitext(name)[1]))
        #elif os.path.isdir(os.path.join(directory, name)):
        #    filelist += getBagFiles(os.path.join(directory, name),filetype)
        else:
            print("Not supported file type")

    if len(filelist)== 0:
        print("No valid files in directory "+str(directory))

    return filelist




# Main function.
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('file_dir', help='Bag file to analyse')
    parser.add_argument('-m', help='Parse multiple bagfiles in folder, use for splitted bags', action="store_true", required=False, default=False)

    args = parser.parse_args()

    if args.m:
        # Check input arguments
        if not os.path.isdir(args.file_dir):
            print("Path needs to be a directory")
            exit(-1)

        filelist = getBagFiles(args.file_dir,['.bag'])

        image_creator = BagReporter(filelist, splitbags=True)
    else:
        if not os.path.isfile(args.file_dir):
            print("Path needs to be a file")
            exit(-1)
        BagReporter(args.file_dir)