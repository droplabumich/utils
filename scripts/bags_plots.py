#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import csv
from collections import defaultdict
import rosbag

from os.path import isfile, join
from os import listdir

def read_bag(inputBagString):
    input_bag =  rosbag.Bag(inputBagString, 'r')
    depth = []
    pose = []
    time=[]
    for topic, msg, t in input_bag.read_messages(topics=['/sphere_a/nav_sensors/pressure_sensor', '/sphere_a/nav/pose_estimation']):
            if topic=='/sphere_a/nav_sensors/pressure_sensor':
                #Convert to North and East (input in decimal degrees)
                depth.append(msg.depth)
                time.append(msg.header.stamp.nsecs)
            if topic == '/sphere_a/nav/pose_estimation':
                    # Convert to North and East (input in decimal degrees)
                    pose.append(msg.pose.pose.position.z)

    return depth, pose, time


def plot_all(folder_path):

    bags = [f for f in listdir(folder_path) if isfile(join(folder_path,f))]

    for bag in bags:
        if not ".txt" in bag:
            data,pose, time = read_bag(join(folder_path,bag))
            print time
            print pose
            plot_csv(data,pose,time,bag)

def read_csv(filename_pose):
    # mpc
    columns_pose = defaultdict(list)  # each value in each column is appended to a list
    #filename_pose = 'depth_2017-09-12-21-20-39.txt'

    with open(filename_pose) as f:
        reader = csv.DictReader(f)  # read rows into a dictionary format
        for row in reader:  # read a row as {column1: value1, column2: value2,...}
            for (k, v) in row.items():  # go over each column name and value
                columns_pose[k].append(v)  # append the value into the appropriate list
                # based on column name k
    print(np.array(columns_pose['field.depth']))

def plot_csv(depth,column_pose,time, bag):
    i = 0
    # time_pose = np.zeros(len(columns_pose['%time']))
    # for item in columns_pose['%time']:
    #     time_pose[i] = float(item)
    #     i = i + 1
    #time = (time - ((time[0]) * np.ones(len(time)))) / 1e9
    #print len(time)
    time = np.zeros(len(depth))
    for i in range(0,len(time)):
        time[i] = i
    if (len(time)>2059):
        begin = 63
        end = 2059
        time[begin:end] = (time[begin:end] - ((time[begin]) * np.ones(len(time[begin:end]))))
    else:
        begin = 0
        end = len(time)


    plt.plot(time[begin:end], depth[begin:end],'-b', label='Depth', linewidth=2.0)
    #plt.axis([63, 160, 0, 0.03])
    #plt.plot(column_pose, 'b', label='Pose depth')
    plt.legend(loc='lower right')
    plt.xlabel('Time (s)')
    plt.ylabel('Position (m)')
    plt.title(bag)
    plt.grid(True)
    plt.show()

if __name__=="__main__":
    plot_all("/media/corinaba/DropLab_Corina/Sphere_ICRA2018/0913/")
