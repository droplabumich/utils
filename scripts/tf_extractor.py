#!/usr/bin/python

from __future__ import division
import rospy
import rosbag
import tf
import tf.transformations
import yaml
import cv2 as cv
import os
from math import pi
import datetime
from geometry_msgs.msg import TransformStamped
import numpy as np


class BagTf:

    def __init__(self, inputBag):

        self.inputBag = rosbag.Bag(inputBag)
        self.start = rospy.Time.from_sec(self.inputBag.get_start_time())
        self.stop = rospy.Time.from_sec(self.inputBag.get_end_time())
        self.interpolate = True

        self.cache_time = (self.stop-self.start) + rospy.Duration(1.0)
        self.tranformer = tf.Transformer(self.interpolate, self.cache_time)
        self.saveTf()
        print(self.tranformer.getFrameStrings())

    def __del__(self):
        self.inputBag.close()

    def saveTf(self):
        count = 0
        for topic, msg, t in self.inputBag.read_messages(topics="/tf"):
            for transform in msg.transforms:
                # if transform.header.frame_id=='odom':
                #    print(transform)
                #    print("*********************")
                # if count >1000:
                #    return
                # else:
                #     count+=1
                self.tranformer.setTransform(transform)
        print("Finished creating tf cache")

    def lookupTf(self, target_frame, base_frame, time):
        try:
            return self.tranformer.lookupTransform(target_frame, base_frame, time)
        except Exception as e:
            print(e.message)


class ImageExtractor:
    def __init__(self, inputBag, _image_topic):

        self.inputBag = rosbag.Bag(inputBag, 'r')
        self.image_topic = _image_topic

        self.timestamps = []
        self.translation = []
        self.rotQtn = []

        # Set up  directory structure
        self.bag_name_we = os.path.basename(inputBag)
        self.bag_name = os.path.splitext(self.bag_name_we)[0]
        self.bag_dir = os.path.dirname(inputBag)
        self.output_dir = os.path.join(
            self.bag_dir, self.bag_name+'_images/img/')
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

        self.date = self.parseBagName()

        # Create a tf cache
        self.tf = BagTf(inputBag)

        # self.bridge = CvBridge()

        # Extract the images
        self.processBag()

    def __del__(self):
        self.inputBag.close()

    def processBag(self):
        imnum = 0
        with open(self.output_dir+'stereo_pose_est.data', mode='w') as structuredFile:
            structuredFile.write("% STEREO_POSE_FILE VERSION 2\n")

            for topic, msg, t in self.inputBag.read_messages(topics=[self.image_topic]):
                if topic == self.image_topic:
                    timestamp = t
                    timestamp = msg.header.stamp
                    if self.tf.lookupTf("RigidBodySphere", "vicon", timestamp) != None:
                        (trans,rot) = self.tf.lookupTf("RigidBodySphere", "vicon", timestamp)
                        self.translation.append(trans)
                        self.rotQtn.append(rot)
                    imnum += 1

    def extractImages(self, structuredFile, imnum, msg):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(msg, "bgr8")
            self.timestamp = msg.header.stamp

            # Prepare image name according to format for structured (PR_date_time_milisec_LC16.png)
            date_string = str(self.date.date()).translate(None, '-')
            time_string = str(datetime.datetime.fromtimestamp(
                self.timestamp.to_sec()).time()).translate(None, ':')
            image_name = "PR_"+date_string+'_' + \
                time_string[0:6]+'_'+time_string[7:10]+'_LC16'+".png"

            # if imnum == 30:
            #   return
            cv.SaveImage(self.output_dir + image_name, cv.fromarray(cv_image))
            self.saveStereoPoseData(structuredFile, image_name, imnum)
            print("Got image "+str(imnum))
        except CvBridgeError, e:
            print e

    def saveStereoPoseData(self, structuredFile, image_name, im_num):

        # Convert camera coordiantes to the structured reference frame
        base_to_odom = self.tf.lookupTf(
            "/odom", "/base_footprint", self.timestamp)
        # odom_to_base = [-x for x in base_to_odom[0]] #Put negatve sign in front
        odom_to_base = [x for x in base_to_odom[0]]

        #euler_base_to_odom = tf.transformations.euler_from_quaternion(base_to_odom[1])
        # print(base_to_odom)
        # print(odom_to_base)

        #print(tuple([(180/pi)*x for x in euler_base_to_odom]))
        base_to_structured = self.tf.lookupTf(
            "structured", "base_footprint", self.timestamp)
        #structured_to_base = [-x for x in base_to_structured[0]]
        structured_to_base = [x for x in base_to_structured[0]]
        # print(structured_to_base)

        camera_to_structured = self.tf.lookupTf(
            "structured", "camera", self.timestamp)
        # print(camera_to_structured)
        euler_camera_to_structured = tf.transformations.euler_from_quaternion(
            camera_to_structured[1])

        euler_base_to_structured = tf.transformations.euler_from_quaternion(
            base_to_structured[1])
        #print(tuple([(180/pi)*x for x in euler_base_to_structured]))
        odom_to_structured = self.tf.lookupTf(
            "structured", "odom", self.timestamp)

        euler_odom_to_structured = tf.transformations.euler_from_quaternion(
            odom_to_structured[1],)
        #print(tuple([(180/pi)*x for x in euler_odom_to_structured]))

        position = llUTM.UTMtoLL(
            23, self.origin.northing+base_to_odom[0][1], self.origin.easting+base_to_odom[0][0], self.origin.zone)

        # if (self.lastlat-position[0])<10**-5 and (self.lastlon-position[1])<10**-5 :
        #   print("Equal")
        # else:
        #   print("Calc GPS Lat: ",position[0],"Calc GPS Lon: ",position[1])
        #  print("True GPS Lat: ", self.lastlat, "True GPS Lon: ", self.lastlon)

        # print(base_to_structured[0])
        # Save to file
        structuredFile.write(str(im_num)+' '+str(self.timestamp))
        structuredFile.write(" "+str(position[0])+" "+str(position[1])+" ")
        #structuredFile.write(str(base_to_structured[0]).translate(None,'(,)')+" ")
        structuredFile.write(
            str(camera_to_structured[0]).translate(None, '[(,)]')+" ")
        structuredFile.write(
            str(euler_camera_to_structured).translate(None, '(,)'))
        structuredFile.write(' '+image_name+' ')
        #structuredFile.write(" fake.png ")
        structuredFile.write(image_name+' ')
        structuredFile.write(str(self.lastdepth)+' ')
        structuredFile.write('0 ')
        structuredFile.write('0 '+'\n')

    def parseBagName(self):

        # Get the date from the current bag file name
        splitted_date = list(int(x) for x in self.bag_name_we[1:11].split('-'))
        splitted_time = list(int(x)
                             for x in self.bag_name_we[12:20].split('-'))

        date = datetime.datetime(
            splitted_date[0], splitted_date[1], splitted_date[2], splitted_time[0], splitted_time[1], splitted_time[2])

        return date


if __name__ == "__main__":
    imExt = ImageExtractor(
        '/home/zhangtianyi/MHLdata/2018_04_17_222408/_2018-04-17-22-24-14_0.bag', "/stereo_in/left/camera_info")
