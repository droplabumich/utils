#!/usr/bin/python

from __future__ import division
import rospy
import rosbag
import tf
import tf.transformations
import os
import datetime
from geometry_msgs.msg import TransformStamped
import numpy as np
import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.backends.backend_pdf import PdfPages
from scipy import signal as sig
from scipy.spatial.transform import Rotation as R
from plot3d import *
from axxbSolver import *


class BagTf:

    def __init__(self, inputBag):

        self.tftimestp = []
        self.inputBag = rosbag.Bag(inputBag)
        self.start = rospy.Time.from_sec(self.inputBag.get_start_time())
        self.stop = rospy.Time.from_sec(self.inputBag.get_end_time())
        self.interpolate = True

        self.cache_time = (self.stop-self.start) + rospy.Duration(1.0)
        self.tranformer = tf.Transformer(self.interpolate, self.cache_time)
        self.saveTf()
        print(self.tranformer.getFrameStrings())

    def __del__(self):
        self.inputBag.close()

    def saveTf(self):
        # time = rospy.Time()
        for topic, msg, t in self.inputBag.read_messages(topics="/tf"):
            # if (t-time) > rospy.Duration(10):
            #     print time-t
            self.tftimestp.append(t)
            for transform in msg.transforms:
                self.tranformer.setTransform(transform)
            # time = t
        # print (time)
        print("Finished creating tf cache")

    def lookupTf(self, target_frame, base_frame, time):
        try:
            return self.tranformer.lookupTransform(target_frame, base_frame, time)
        except Exception as e:
            print(e.message)


class ImageExtractor:
    def __init__(self, inputBag, _image_topic, storeRT, mask):

        self.inputBag = rosbag.Bag(inputBag, 'r')
        self.image_topic = _image_topic

        self.timestamps = []
        self.translation = []
        self.rotEuler = []

        # Set up  directory structure
        self.bag_name_we = os.path.basename(inputBag)
        self.bag_name = os.path.splitext(self.bag_name_we)[0]
        self.bag_dir = os.path.dirname(inputBag)
        self.output_dir = os.path.join(
            self.bag_dir, self.bag_name+'_images/img/')
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

        # Create a tf cache
        self.tf = BagTf(inputBag)

        # Extract the images
        self.processBag(storeRT, mask)

    def __del__(self):
        self.inputBag.close()

    def processBag(self, storeRT, mask):
        imnum = 0
        with open(self.output_dir+'stereo_pose_est.data', mode='w') as structuredFile:
            structuredFile.write("% STEREO_POSE_FILE VERSION 2\n")
            for topic, msg, t in self.inputBag.read_messages(topics=[self.image_topic]):
                if topic == self.image_topic:
                    lastt = rospy.Time()
                    timestamp = t
                    for tftopic, tfmsg, tft in self.inputBag.read_messages(topics="/tf"):
                        if tft > timestamp:
                            break
                        lastt = tft

                    if timestamp - lastt > rospy.Duration(1):
                        mask.append(0)
                    else:
                        mask.append(1)

                    if self.tf.lookupTf("RigidBodySphere", "vicon", timestamp) != None:
                        (trans, rot) = self.tf.lookupTf(
                            "RigidBodySphere", "vicon", timestamp)
                        self.translation.append(trans)
                        trans[0] = 0.3048*trans[0]
                        trans[1] = 0.3048*trans[1]
                        trans[2] = 0.3048*trans[2]
                        rr = R.from_quat(rot)
                        # print("rot converted", rr.as_dcm())
                        self.rotEuler.append(rr.as_dcm())
                        self.timestamps.append(timestamp)
                    imnum += 1
            # caculate incremental
            for i in range(1, len(self.translation)):
                storeRT.optT.append([self.translation[i][0]-self.translation[i-1][0], self.translation[i]
                                     [1]-self.translation[i-1][1], self.translation[i][2]-self.translation[i-1][2]])
                incR = R.from_dcm(
                    np.matmul(self.rotEuler[i], np.transpose(self.rotEuler[i-1])))
                storeRT.optEuler.append(incR.as_rotvec())
                # print(storeRT.optEuler[i-1])
            # print len(self.translationInc), self.rotEulerInc


class odomLogReader:
    def __init__(self, odoLogPath, storeRT):
        self.R = []
        with open(odoLogPath) as csv_file:
            csv_reader = csv.reader(csv_file)
            line_count = 0
            for row in csv_reader:
                if line_count > 4:
                    self.R.append(
                        np.array(map(float, row[0:9])).reshape((3, 3)))
                    storeRT.odoT.append(np.array(map(float, row[9:12])))
                    # tempr = R.from_dcm(np.array(map(float, row[0:9])).reshape((3, 3)))
                    # storeRT.odoEuler.append(tempr.as_euler())
                    storeRT.odoEuler.append(tf.transformations.euler_from_matrix(
                        np.array(map(float, row[0:9])).reshape((3, 3)), 'syxz'))
                    storeRT.kp.append(np.array(map(float, row[12:14])))
                    storeRT.Matches.append(np.array(map(float, row[14:17])))
                    storeRT.reprojErr.append(map(float, row[17:18]))
                line_count += 1
                # print storeRT.odoT


class KFLogReader:
    def __init__(self, KFLogPath, storeRT):
        kfindex = []
        with open(KFLogPath) as csv_file:
            csv_reader = csv.reader(csv_file)
            line_count = 0
            for row in csv_reader:
                if line_count > 4:
                    storeRT.KFodoT.append(np.array(map(float, row[9:12])))
                    storeRT.KFodoEuler.append(tf.transformations.euler_from_matrix(
                        np.array(map(float, row[0:9])).reshape((3, 3)), 'syxz'))
                    storeRT.KFreprojErr.append(map(float, row[17:18]))
                    kfindex.append(map(float, row[18:19]))
                line_count += 1
        framePerKF = np.array(kfindex)
        framePerKF[1:len(framePerKF)] = np.array(kfindex[1:len(
            framePerKF)])-np.array(kfindex[0:len(framePerKF)-1])
        # print(framePerKF)

        print(len(kfindex))
        print(len(storeRT.KFodoT))
        print(np.size(framePerKF))

        for i in range(0, len(kfindex)):
            for j in range(0, framePerKF[i]):
                storeRT.KFodoTFull.append(storeRT.KFodoT[i]/framePerKF[i])
                storeRT.KFodoEulerFull.append(
                    storeRT.KFodoEuler[i]/framePerKF[i])


class visualizer:
    def __init__(self, RT, fig3, pdfname, mask):
        self.optR = np.array(RT.optEuler)
        self.optT = np.array(RT.optT)

        self.optR[:, 0] = sig.medfilt(self.optR[:, 0], 5)
        self.optR[:, 1] = sig.medfilt(self.optR[:, 1], 5)
        self.optR[:, 2] = sig.medfilt(self.optR[:, 2], 5)
        self.optT[:, 0] = sig.medfilt(self.optT[:, 0], 5)
        self.optT[:, 1] = sig.medfilt(self.optT[:, 1], 5)
        self.optT[:, 2] = sig.medfilt(self.optT[:, 2], 3)

        for i in range(0, len(RT.optT)):
            # if mask[i] == 1:
            if mask[i] == 1 and abs(self.optR[i, 0]) < 0.1 and abs(self.optR[i, 1]) < 0.1 and abs(self.optR[i, 2]) < 0.8 and abs(self.optT[i, 0]) < 0.1 and abs(self.optT[i, 1]) < 0.8 and abs(self.optT[i, 2]) < 0.1:
                pass
            else:
                mask[i] = 0

        self.indexToShow = []
        for i in range(0, len(mask)):
            if mask[i] == 1 and i < len(RT.optEuler):
                self.indexToShow.append(i)

        self.odoR = np.array(RT.odoEuler)
        self.odoT = np.array(RT.odoT)
        self.KFodoR = np.array(RT.KFodoEulerFull)
        self.KFodoT = np.array(RT.KFodoTFull)

        self.errR = self.optR - \
            self.odoR[0:self.optR.shape[0], 0:self.optR.shape[1]]
        self.errT = self.optT - \
            self.odoT[0:self.optT.shape[0], 0:self.optT.shape[1]]

        navy = navySolver()
        self.optR, self.optT = navy.calibrateR(
            self.optR, self.odoR, self.optT, self.odoT, self.indexToShow)

        odoErrRRMS = self.caculateErrRMS(self.indexToShow, self.odoR, self.optR)
        odoErrTRMS = self.caculateErrRMS(self.indexToShow, self.odoT, self.optT)

        KFodoErrRRMS = self.caculateErrRMS(self.indexToShow, self.KFodoR, self.optR)
        KFodoErrTRMS = self.caculateErrRMS(self.indexToShow, self.KFodoT, self.optT)


        fig = plt.figure(figsize=(14, 10))
        ax1 = plt.subplot(321)
        plt.title('Measurement Comparison in Translation')
        line2, = plt.plot(
            self.odoT[0:self.optR.shape[0], 0], 'c', linewidth=1.5)
        line3, = plt.plot(
            self.KFodoT[0:self.optR.shape[0], 0], 'r', linewidth=1)
        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                line1, = plt.plot(np.arange(lastHead, lasti+1),
                                  self.optT[lastHead:lasti+1, 0], 'b', linewidth=1.5)
                lastHead = i
            lasti = i
        line1.set_label('optitrack')
        line2.set_label('odometry')
        line3.set_label('KeyFrame Odometry')
        plt.axhline(0, color='black')
        # plt.setp(ax1.get_xticklabels(), visible=False)
        plt.ylabel("x(m)")
        ax1.legend(loc='best')

        ax2 = plt.subplot(323, sharex=ax1)
        plt.plot(self.odoT[0:self.optR.shape[0], 1], 'c', linewidth=1.5)
        plt.plot(self.KFodoT[0:self.optR.shape[0], 1], 'r', linewidth=1)
        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                line1, = plt.plot(np.arange(lastHead, lasti+1),
                                  self.optT[lastHead:lasti+1, 1], 'b', linewidth=1.5)
                lastHead = i
            lasti = i

        plt.axhline(0, color='black')
        plt.ylabel("y(m)")

        ax3 = plt.subplot(325, sharex=ax1)
        plt.plot(self.odoT[0:self.optR.shape[0], 2], 'c', linewidth=1.5)
        plt.plot(self.KFodoT[0:self.optR.shape[0], 2], 'r', linewidth=1)
        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                line1, = plt.plot(np.arange(lastHead, lasti+1),
                                  self.optT[lastHead:lasti+1, 2], 'b', linewidth=1.5)
                lastHead = i
            lasti = i
        plt.axhline(0, color='black')
        plt.ylabel("z(m)")
        plt.xlabel("frame")

        ax4 = plt.subplot(322)
        plt.title('Measurement Comparison in Rotation')
        plt.plot(self.odoR[0:self.optR.shape[0], 0], 'c', linewidth=1.5)
        plt.plot(self.KFodoR[0:self.optR.shape[0], 0], 'r', linewidth=1)
        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                line1, = plt.plot(np.arange(lastHead, lasti+1),
                                  self.optR[lastHead:lasti+1, 0], 'b', linewidth=1.5)
                lastHead = i
            lasti = i
        plt.axhline(0, color='black')
        plt.ylabel("roll(rad)")

        ax5 = plt.subplot(324, sharex=ax1)
        plt.plot(self.odoR[0:self.optR.shape[0], 1], 'c', linewidth=1.5)
        plt.plot(self.KFodoR[0:self.optR.shape[0], 1], 'r', linewidth=1)

        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                line1, = plt.plot(np.arange(lastHead, lasti+1),
                                  self.optR[lastHead:lasti+1, 1], 'b', linewidth=1.5)
                lastHead = i
            lasti = i
        plt.axhline(0, color='black')
        plt.ylabel("pitch(rad)")

        ax6 = plt.subplot(326, sharex=ax1)
        plt.plot(self.odoR[0:self.optR.shape[0], 2], 'c', linewidth=1.5)
        plt.plot(self.KFodoR[0:self.optR.shape[0], 2], 'r', linewidth=1)
        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                line1, = plt.plot(np.arange(lastHead, lasti+1),
                                  self.optR[lastHead:lasti+1, 2], 'b', linewidth=1.5)
                lastHead = i
            lasti = i
        plt.axhline(0, color='black')
        plt.ylabel("yaw(rad)")
        plt.xlabel("frame")

        pp = PdfPages(pdfname)
        pp.savefig(fig)

        fig2 = plt.figure(figsize=(14, 10))
        ax1 = plt.subplot(321)
        plt.title('Errors Comparison in Translation')
        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                line1, = plt.plot(np.arange(lastHead, lasti+1), self.optT[lastHead:lasti+1, 0] -
                                  self.odoT[lastHead:lasti+1, 0], 'c', linewidth=1)
                line2, = plt.plot(np.arange(lastHead, lasti+1), self.optT[lastHead:lasti+1, 0] -
                                  self.KFodoT[lastHead:lasti+1, 0], 'r', linewidth=1)
                lastHead = i
            lasti = i
        plt.axhline(0, color='black')
        plt.axhline(odoErrTRMS[0], color='c')
        plt.axhline(KFodoErrTRMS[0], color='r')
        line1.set_label('non-KF Error and RMS')
        line2.set_label('KeyFrame Error and RMS')
        ax1.legend(loc='best')
        # plt.setp(ax1.get_xticklabels(), visible=False)
        plt.ylabel("x(m)")

        ax2 = plt.subplot(323, sharex=ax1)
        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                plt.plot(np.arange(lastHead, lasti+1), self.optT[lastHead:lasti+1, 1] -
                         self.odoT[lastHead:lasti+1, 1], 'c', linewidth=1)
                plt.plot(np.arange(lastHead, lasti+1), self.optT[lastHead:lasti+1, 1] -
                         self.KFodoT[lastHead:lasti+1, 1], 'r', linewidth=1)
                lastHead = i
            lasti = i
        plt.axhline(0, color='black')
        plt.axhline(odoErrTRMS[1], color='c')
        plt.axhline(KFodoErrTRMS[1], color='r')
        plt.ylabel("y(m)")

        ax3 = plt.subplot(325, sharex=ax1)
        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                plt.plot(np.arange(lastHead, lasti+1), self.optT[lastHead:lasti+1, 2] -
                         self.odoT[lastHead:lasti+1, 2], 'c', linewidth=1)
                plt.plot(np.arange(lastHead, lasti+1), self.optT[lastHead:lasti+1, 2] -
                         self.KFodoT[lastHead:lasti+1, 2], 'r', linewidth=1)
                lastHead = i
            lasti = i
        plt.axhline(0, color='black')
        plt.axhline(odoErrTRMS[2], color='c')
        plt.axhline(KFodoErrTRMS[2], color='r')
        plt.ylabel("z(m)")
        plt.xlabel("frame")

        ax4 = plt.subplot(322)
        plt.title('Errors Comparison in Rotation')
        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                plt.plot(np.arange(lastHead, lasti+1), self.optR[lastHead:lasti+1, 0] -
                         self.odoR[lastHead:lasti+1, 0], 'c', linewidth=1)
                plt.plot(np.arange(lastHead, lasti+1), self.optR[lastHead:lasti+1, 0] -
                         self.KFodoR[lastHead:lasti+1, 0], 'r', linewidth=1)
                lastHead = i
            lasti = i
        plt.axhline(0, color='black')
        plt.axhline(odoErrRRMS[0], color='c')
        plt.axhline(KFodoErrRRMS[0], color='r')
        plt.ylabel("roll(rad)")

        ax5 = plt.subplot(324, sharex=ax1)
        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                plt.plot(np.arange(lastHead, lasti+1), self.optR[lastHead:lasti+1, 1] -
                         self.odoR[lastHead:lasti+1, 1], 'c', linewidth=1)
                plt.plot(np.arange(lastHead, lasti+1), self.optR[lastHead:lasti+1, 1] -
                         self.KFodoR[lastHead:lasti+1, 1], 'r', linewidth=1)
                lastHead = i
            lasti = i
        plt.axhline(0, color='black')
        plt.axhline(odoErrRRMS[1], color='c')
        plt.axhline(KFodoErrRRMS[1], color='r')
        plt.ylabel("pitch(rad)")

        ax6 = plt.subplot(326, sharex=ax1)
        lastHead = 0
        lasti = 0
        for i in self.indexToShow:
            if i-lasti > 1 or self.indexToShow.index(i) == len(self.indexToShow)-1:
                plt.plot(np.arange(lastHead, lasti+1), self.optR[lastHead:lasti+1, 2] -
                         self.odoR[lastHead:lasti+1, 2], 'c', linewidth=1)
                plt.plot(np.arange(lastHead, lasti+1), self.optR[lastHead:lasti+1, 2] -
                         self.KFodoR[lastHead:lasti+1, 2], 'r', linewidth=1)
                lastHead = i
            lasti = i
        plt.axhline(0, color='black')
        plt.axhline(odoErrRRMS[2], color='c')
        plt.axhline(KFodoErrRRMS[2], color='r')
        plt.ylabel("yaw(rad)")
        plt.xlabel("frame")

        pp.savefig(fig2)

        fig = plt.figure(figsize=(14, 4.4))
        plt.title('All frame Reprojection Error')
        line1, = plt.plot(RT.reprojErr[:], 'b', linewidth=1.5)
        plt.ylabel("pixels")
        plt.xlabel("frame")
        line1.set_label('Reprojection Error')
        plt.ylim((0,20))
        plt.legend(loc='best')
        pp.savefig(fig)

        fig = plt.figure(figsize=(14, 4.4))
        plt.title('Keyframe Reprojection Error')
        line1, = plt.plot(RT.KFreprojErr[:], 'b', linewidth=1.5)
        plt.ylabel("pixels")
        plt.xlabel("frame")
        line1.set_label('Reprojection Error')
        plt.legend(loc='best')
        pp.savefig(fig)

        tempkp = []
        tempmatch = []
        for i in range(0, len(mask)):
            # if mask[i] == 1:
            tempkp.append(RT.kp[i])
            tempmatch.append(RT.Matches[i])

        fig = plt.figure(figsize=(14, 6))
        ax1 = plt.subplot(121)
        ax1.set_yscale('log')
        plt.title('Num of Features')
        line = plt.plot(tempkp, linewidth=1.5)
        plt.ylabel("Num of features")
        plt.xlabel("frame")
        plt.legend(iter(line), ('RightFrame', 'LeftFrame'), loc='best')

        ax2 = plt.subplot(122)
        ax2.set_yscale('log')
        plt.title('Num of Matches')
        line = plt.plot(tempmatch, linewidth=1.5)
        plt.ylabel("Num of Matches")
        plt.xlabel("frame")
        plt.legend(iter(line), ('left2right', 'last2current',
                                'RANSAC Inliers'),  loc='best')

        pp.savefig(fig)
        pp.savefig(fig3)
        pp.close()
    
    def caculateErrRMS(self, idxToShow, data1, data2):
        # rms = np.zeros(data.shape[1])
        rms = np.sqrt(np.mean(np.square(data1[idxToShow,:]-data2[idxToShow,:]),axis=0))
        return rms
        # for i in idxToShow:





class storageRT:
    def __init__(self):
        self.odoEuler = []
        self.odoT = []
        self.optEuler = []
        self.optT = []

        self.odoErrRRMS = []
        self.odoErrTRMS = []

        self.kp = []
        self.Matches = []
        self.reprojErr = []
        self.KFreprojErr = []

        self.KFodoT = []
        self.KFodoEuler = []
        self.KFodoTFull = []
        self.KFodoEulerFull = []

        self.KFodoErrRRMS = []
        self.KFodoErrTRMS = []

if __name__ == "__main__":
    outlierMask = []
    RT = storageRT()
    imExt = ImageExtractor(
        '/home/zhangtianyi/MHLdata/2018_04_17_222408/_2018-04-17-22-24-14_0.bag', "/stereo_in/left/camera_info", RT, outlierMask)
    # print len(outlierMask)
    imExt = ImageExtractor(
        '/home/zhangtianyi/MHLdata/2018_04_17_222408/_2018-04-17-22-25-56_1.bag', "/stereo_in/left/camera_info", RT, outlierMask)
    # print len(outlierMask)
    odoReader = odomLogReader(
        '/home/zhangtianyi/visual-odometry-custom/2018_04_17_222408.csv', RT)
    KFodoReader = KFLogReader(
        '/home/zhangtianyi/visual-odometry-custom/kf_2018_04_17_222408.csv', RT)
    plot3d = plot3d(outlierMask)
    fig3 = plot3d.getfigure()
    pdfname = '/home/zhangtianyi/MHLdata/2018_04_17_222408.pdf'
    vis = visualizer(RT, fig3, pdfname, outlierMask)
