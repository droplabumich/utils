#!/usr/bin/env python

# this is the tool for converting img format in folder

import cv2 as cv
import os
from os import listdir,makedirs
from os.path import isfile,join
from cv_bridge import CvBridge, CvBridgeError

path = '/home/zhangtianyi/catkin_ws/src/VINS-Fusion/camera_models/camera_calib_example/simsunderwater/lcCvtg' # Source Folder
dstpath = '/home/zhangtianyi/catkin_ws/src/VINS-Fusion/camera_models/camera_calib_example/simsunderwater/lcCvtgRenamed' # Destination Folder
prefix = 'left-'
try:
    makedirs(dstpath)
except:
    print ("Directory already exist, images will be written in SAme folder")

# Folder won't used
files = [f for f in listdir(path) if isfile(join(path,f))] 

for image in files:
    # try:
    img = cv.imread(str(path)+'/'+str(image),0)
    color = cv.cvtColor(img, cv.COLOR_BayerRG2RGB)
    gray = cv.cvtColor(color, cv.COLOR_RGB2GRAY)
    dstPath = join(dstpath,prefix,image)

    cv.imwrite(dstPath,gray)