#!/usr/bin/python

import csv
import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from icp import *


class plot3d:
    def __init__(self):

        R = []
        T = []

        with open("/home/zhangtianyi/visual-odometry-custom/hawaii.csv") as csv_file:
            csv_reader = csv.reader(csv_file)
            line_count = 0
            R.append(np.eye(3))
            for row in csv_reader:
                if line_count > 4:
                    R.append(np.array(map(float, row[0:9])).reshape((3, 3)))
                    T.append(np.array(map(float, row[9:12])))
                line_count += 1

        R_global = np.array(R)
        T_global = np.array(T)

        for i in range(1, line_count-5):
            R_global[i] = np.matmul(R_global[i, :, :], R_global[i-1, :, :])
            T_global[i, :] = T_global[i-1, :] + \
                np.matmul(R_global[i-1, :, :], T_global[i, :])

        ###############################################################

        self.fig = plt.figure(figsize=(14,12))

        ax = self.fig.add_subplot(2,2,1,projection='3d')
        line2, = ax.plot(T_global[:,0], T_global[:,1], -T_global[:,2])
        ax.view_init(10,45)
        # ax.set_zlim(-3,3)
        ax.set_xlabel("x(m)")
        ax.set_ylabel("y(m)")
        ax.set_zlabel("depth(m)")
        ax.axis('equal')
        line2.set_label('Visual_Odometry')
        ax.legend(loc='best')

        ax = self.fig.add_subplot(2,2,2,projection='3d')
        line2, = ax.plot(T_global[:,0], T_global[:,1], -T_global[:,2])
        ax.view_init(15,135)
        # ax.set_zlim(-3,3)
        ax.set_xlabel("x(m)")
        ax.set_ylabel("y(m)")
        ax.set_zlabel("depth(m)")
        ax.axis('equal')
        line2.set_label('Visual_Odometry')
        ax.legend(loc='best')

        ax = self.fig.add_subplot(2,2,3,projection='3d')
        line2, = ax.plot(T_global[:,0], T_global[:,1], -T_global[:,2])
        ax.view_init(35,225)
        # ax.set_zlim(-3,3)
        ax.set_xlabel("x(m)")
        ax.set_ylabel("y(m)")
        ax.set_zlabel("depth(m)")
        ax.axis('equal')
        line2.set_label('Visual_Odometry')
        ax.legend(loc='best')

        ax = self.fig.add_subplot(2,2,4,projection='3d')
        line2, = ax.plot(T_global[:,0], T_global[:,1], -T_global[:,2])
        ax.view_init(50,315)
        # ax.set_zlim(-3,3)
        ax.set_xlabel("x(m)")
        ax.set_ylabel("y(m)")
        ax.set_zlabel("depth(m)")
        ax.axis('equal')
        line2.set_label('Visual_Odometry')
        ax.legend(loc='best')
        # print("plot3d done")
        # plt.show()

    def getfigure(self):
        return self.fig
